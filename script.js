// determine order of menu items and which one should be active
// table contains identifier, webpage, menu entry and page title
var items = [["indx", "index.html", "Home", "The Combinatorial Object Server++"],
             ["perm", "perm", "Permutations", "Permutations"],
             ["multiperm", "multiperm", "Multiperms", "Multiset permutations"],
             ["meander", "meander", "Meanders", "Meanders and stamp foldings"],
             ["comb", "comb", "Combinations", "Combinations"],
             ["bits", "bits", "Bitstrings", "Bitstrings"],
             ["middle", "middle", "Middle levels", "Middle levels"],
             ["part", "part", "Partitions", "Integer and set partitions"],
             ["dyck", "dyck", "Lattice paths", "Lattice paths"],
             ["tree", "tree", "Plane trees", "Rooted and free plane trees"],
             ["span", "span", "Spanning trees", "Spanning trees"],
             ["necklace", "necklace", "Necklaces", "Necklaces and Lyndon words"],
             ["bruijn", "bruijn", "De Bruijn sequences", "De Bruijn sequences"],
             ["sgroup", "sgroup", "Semigroups", "Numerical semigroups"]];

var my_class = document.getElementById("nav0").getAttribute("class");
var nav_string = "<ul id='menu'>";
for (var i = 0; i < items.length; i++) {
  active_string = "";
  if (my_class == items[i][0]) {
    active_string = "class = 'active' ";
    document.title = items[i][3];
  }
  nav_string += "<li id='menu_item'><a " + active_string + "href='" + items[i][1] + "'>" + items[i][2] + "</a></li>";
}
nav_string += "</ul>";
document.getElementById("nav0").innerHTML = nav_string;

// make section headings collapsible
var heads = document.getElementsByClassName("collapse_heading");
for (var i = 0; i < heads.length; i++) {
  heads[i].innerHTML = "&#9660; " + heads[i].innerHTML;
  heads[i].addEventListener("click", function() {
    var sec = this.nextElementSibling;
    if (sec.style.maxHeight){
      this.innerHTML = "&#9660; "+ this.innerHTML.slice(2);
      sec.style.maxHeight = null;
    } else {
      this.innerHTML = "&#9650; "+ this.innerHTML.slice(2);
      sec.style.maxHeight = sec.scrollHeight + "px";
    }
  });
}

// enlarge output generation window on first generation
var enlarge_output = (function() {
  var executed = false;
  return function() {
    if (!executed) {
      var selfmt = document.getElementById("fmt");
      if (selfmt == null) {
        return;  // output format unknown, output window height unchanged
      }
      fmt = selfmt.options[selfmt.selectedIndex].value;  
      if (fmt == 2) {
        return;  // file output, output window height unchanged
      }
      // graphical or textual output, so output window height must be enlarged
      var divout = document.getElementById("output");
      divout.style.height = "500px";
      executed = true;
    }
  };
})();

// update output formatting buttons
function output_buttons()
{
  var selfmt = document.getElementById("fmt");
  if (selfmt == null) {
    return;
  }
  fmt = selfmt.options[selfmt.selectedIndex].value;
  var opt = document.getElementById("options");
  if (fmt == 0) {  // graphical output, show options
    opt.style.display = "";
  } else if (fmt == 1) {  // textual output, hide options
    opt.style.display = "none";
  }  // else file output, no change
}

// switch numbers of computed objects on or off
function numbering()
{
  var checked = document.getElementById("numbering").checked;
  var iframe = document.getElementById("consoleframe");
  var numbers = iframe.contentWindow.document.getElementsByClassName("num");
  for (var i = 0; i < numbers.length; i++) {
    if (checked) {
      numbers[i].style.display = "";
    } else {
      numbers[i].style.display = "none";
    }
  }
}

// switch graphical output represenation of objects on or off
function graphics()
{
  var checked = document.getElementById("graphics").checked;
  var iframe = document.getElementById("consoleframe");
  var strings = iframe.contentWindow.document.getElementsByClassName("txt");
  var graphics = iframe.contentWindow.document.getElementsByClassName("gfx");
  for (var i = 0; i < strings.length; i++) {
    if (checked) {
      strings[i].style.display = "none";
      graphics[i].style.display = "";
    } else {
      strings[i].style.display = "";
      graphics[i].style.display = "none";
    }
  }
}
