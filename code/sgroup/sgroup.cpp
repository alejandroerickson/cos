/*
 * Copyright (c) 2018 Maria Bras-Amoros, Julio Fernandez-Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstring>
#include <iostream>
#include <getopt.h>

#define MAX 150

// recursive tree-like computation of binary right generator descending (RGD) sequences
// as described in the paper by Bras-Amoros and Fernandez-Gonzalez
void rgd(int D[], int m, int mfinal, int c, int cfinal, int g, int gfinal, int efinal, long long &ct, long long ctmax) {
  if ((ctmax >= 0) && (ct == ctmax+1)) {
    return;
  }
  
  if (g == gfinal) {
    // found semigroup with correct genus
    // check if multiplicity and conductor are correct
    if (((mfinal != -1) && (m != mfinal)) || ((cfinal != -1) && (c != cfinal))) {
      return;  // discard semigroups with wrong parameters
    }
    // check if embedding dimension is correct
    if (efinal != -1) {
      // compute the embedding dimension (it is not tracked recursively)
      int e = 1;
      for (int r=1; r<c-m; r++) {
        if (!D[r]) {
          int l;
          for (l=0; l<r && (D[l] || r-l-m<0 || (r-l-m>=0 && D[r-l-m])); ++l);
          if (l>=r) {
            e++;
          }
        }
      }
      for (int r=c-m; r<c; r++) {
        if (D[r]) {
          e++;
        }
      }
      if (e != efinal) {
        return;  // discard semigroups with wrong parameters
      }
    }
    // found semigroup matching all parameters, so count and output it
    ct++;
    if ((ctmax >= 0) && (ct == ctmax+1)) {
      std::cout << "output limit reached" << std::endl;
      return;
    }
    std::cout << "0";
	  for (int l=0; l<c-m; l++) {
	    if (!D[l]) {
	      std::cout << " " << l+m;
      }
	  }
    std::cout << " " << c << std::endl;
    return;
  }

  int newD[MAX];
  memset(newD+c, 0, sizeof(int) * m);
  memcpy(newD, D, sizeof(int) * c);

  for (int r=c-m; r<c; r++) {
    if (newD[r]) {
      int newfrob = r+m;
      int newc = newfrob+1;
      int newm;
      if (r==0) {
        newm = newc;
      } else {
        newm = m;
      }
      int l = r/2;
      while (l>=1 && (newD[l] || newD[r-l])) {
        l--;
      }

      if (l==0) {
   	    newD[newfrob] = 1;
      }

      rgd(newD, newm, mfinal, newc, cfinal, g+1, gfinal, efinal, ct, ctmax);

      newD[r] = 0;
      newD[newfrob] = 0;
    }
  }

  return;
}

// display help
void help() {
  std::cout << "./sgroup [options]  generate numerical semigroups with given genus" << std::endl;
  std::cout << "-h                  display this help" << std::endl;
  std::cout << "-g{0,1,...}         given genus (=number of gaps)" << std::endl;
  std::cout << "-m{1,2,...}         given multiplicity (=smallest non-zero non-gap)" << std::endl;
  std::cout << "-c{0,1,...}         given conductor (=smallest non-gap larger than all gaps)" << std::endl;
  std::cout << "-e{1,2,...}         given embedding dimension (=cardinality of smallest set of generators)" << std::endl;
  std::cout << "-l{-1,0,1,2,...}    number of semigroups to list; -1 for all" << std::endl;
  std::cout << "examples:  ./sgroup -g4" << std::endl;
  std::cout << "examples:  ./sgroup -g4 -m3" << std::endl;
  std::cout << "examples:  ./sgroup -g4 -c8" << std::endl;
  std::cout << "examples:  ./sgroup -g4 -c8 -e2" << std::endl;
  std::cout << "           ./sgroup -g10 -l20" << std::endl;
}

void opt_g_missing() {
  std::cerr << "option -g is mandatory" << std::endl;
}

int main(int argc, char *argv[]) {
  int g;
  int genus;  // the genus
  int mult = -1;  // the multiplicity (-1 if not specified)
  int cond = -1;  // the conductor (-1 if not specified)
  int edim = -1;  // the embedding dimension (-1 if not specified)
  bool g_set = false;  // flag whether option -g is present
  long long limit = -1;  // compute all semigroups with given genus by default

  // process command line options
  int c;
  while ((c = getopt(argc, argv, ":hg:m:c:e:l:")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
      case 'g':
        g = atoi(optarg);
        if (g < 0) {
          std::cerr << "option -g must be followed by an integer from {0,1,...}" << std::endl;
          return 1;
        }
        genus = g;
        g_set = true;
        break;
      case 'm':
        mult = atoi(optarg);
        if (mult < 1) {
          std::cerr << "option -m must be followed by an integer from {1,2,...}" << std::endl;
          return 1;
        }
        break;        
      case 'c':
        cond = atoi(optarg);
        if (cond < 0) {
          std::cerr << "option -c must be followed by an integer from {0,1,...}" << std::endl;
          return 1;
        }
        break;
      case 'e':
        edim = atoi(optarg);
        if (edim < 1) {
          std::cerr << "option -e must be followed by an integer from {1,2,...}" << std::endl;
          return 1;
        }
        break;         
      case 'l':
        limit = atoi(optarg);
        if (limit < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;
      case ':':
        std::cerr << "option -" << (char) optopt << " requires an operand" << std::endl;
        return 1;
      case '?':
        std::cerr << "unrecognized option -" << (char) optopt << std::endl;
        return 1;
    }
  }
  if (!g_set) {
    opt_g_missing();
    help();
    return 1;
  }

  if (genus == 0) {
    if (limit == 0) {
      std::cout << "output limit reached" << std::endl;
    } else if (((mult == -1) || (mult == 1)) && ((cond == -1) || (cond == 0)) && ((edim == -1) || (edim == 1))) {
      std::cout << "0 1" << std::endl;
    }
  } else {
    int D[MAX];
	  D[0] = 1;
  	D[1] = 1;
    int m = 2;
    int c = 2;
    int g = 1;
    long long ct = 0;
	  rgd(D, m, mult, c, cond, g, genus, edim, ct, limit);
  }

  return 0;
}
