/*
 * Copyright (c) 2016 Torsten Muetze, Christoph Standke, Veit Wiechert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DYCK_HH
#define DYCK_HH

#include <vector>

// A class representing a Dyck path that allows performing
// applications of the minimum-change bijection f described
// in the paper [Muetze, Standke, Wiechert] and its inverse f^{-1}
// in constant time.

class DyckPath {
public:
  // constructor
  explicit DyckPath(const std::vector<int>& x);

  // return direct reference to the step vector representing the Dyck path
  const std::vector<int>& get_steps() const { return steps_; }
  // return direct reference to vector of flip positions
  const std::vector<int>& get_flip_pos() const { return flip_pos_; }
  // allow step vector access directly via the Dyck path object
  int& operator[](int i) { return steps_[i]; }
  const int& operator[](int i) const { return steps_[i]; }
  // the number of steps of the Dyck path
  int size() { return steps_.size(); }
  const int size() const { return steps_.size(); }

  int get_num_flaws() const { return num_flaws_; }

  // count the number of upsteps/downsteps starting at the given height
  int count_usteps(int height) const;
  int count_dsteps(int height) const;

  // apply the minimum-change bijection f
  // (has no effect if flaw value is already maximal)
  void apply_f();
  // apply the minimum-change bijection f^{-1}
  // (has no effect if number of flaws is zero)
  void apply_finv();

private:
  // the step sequence of the Dyck path, where 1=upstep and 0=downstep
  std::vector<int> steps_;
  // vector of flip positions for applying f and f^{-1},
  // the entries of this vector form an alternating permutation
  // (the entries are 0-based, whereas in the paper [Muetze,Standke,Wiechert]
  // they are 1-based)
  std::vector<int> flip_pos_;
  // number of flaws (=number of downsteps below the line y=0)
  int num_flaws_;

  // count given step type (1=upstep or 0=downstep) starting at the given height
  int count_steps(int step, int height) const;

  // for the given Dyck path with zero flaws, compute the
  // alternating permutation of flip positions
  void compute_flip_pos(const std::vector<int> &x0);
  // recursion to perform the before-mentioned computation
  // given an array of auxiliary bidirectional pointers next_step
  void compute_flip_pos_rec(const std::vector<int> &x0, int l, int r, int a, bool forward, const std::vector<int> &next_step);
};

// allow comparison between Dyck paths (by comparing the corresponding step vectors)
inline bool operator==(const DyckPath& lhs, const DyckPath& rhs) { return lhs.get_steps() == rhs.get_steps(); }
inline bool operator!=(const DyckPath& lhs, const DyckPath& rhs) { return !operator==(lhs, rhs); }

#endif
