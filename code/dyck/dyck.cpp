/*
 * Copyright (c) 2016 Torsten Muetze, Christoph Standke, Veit Wiechert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <vector>
#include "dyck.hpp"

DyckPath::DyckPath(const std::vector<int>& x) : steps_(x) {
  int length = x.size();
  // path should have even length
  assert(length % 2 == 0);

  // initialize number of flaws
  num_flaws_ = 0;
  int height = 0;
  for (int i = 0; i < length; ++i) {
    if ((steps_[i] == 0) && (height <= 0)) {
      ++num_flaws_;
    }
    height += (2*steps_[i] - 1);
  }
  assert(height == 0);  // number of 0's and 1's should match

  // We want to compute the Dyck path x0 with zero flaws
  // for which f^e(x)=x0 (x is the current Dyck path).

  // positions which are flipped in x0 to reach x
  std::vector<int> flipped_pos;

  // (1) Compute positions of downsteps in x0 that are flipped to upsteps in x.
  //     These are all upsteps in x below the line y=0.
  // (2) Compute positions of upsteps in x0 that are flipped to downsteps in x.
  //     These are (2a) all downsteps in x below the line y=-1 and
  //               (2b) all downsteps below the line y=1 to the left of the
  //                    (d0(x)+1)-th downstep of x that touches the line y=0.
  int num_flipped_usteps = 0;
  int num_flipped_dsteps = 0;
  height = 0;
  int d0 = this->count_dsteps(0);
  int c = 0;
  for (int i = 0; i < length; ++i) {
    bool dstep_touches_0 = (steps_[i] == 0) && ((height == 1) || (height == 0));
    //  (1)+(2a)          (2b)
    if ((height <= -1) || ((c < d0) && dstep_touches_0)) {
      if (steps_[i] == 0) {
        ++num_flipped_dsteps;
      } else {
        ++num_flipped_usteps;
      }
      flipped_pos.push_back(i);
    }
    if (dstep_touches_0) {
      ++c;
    }
    height += (2*steps_[i] - 1);
  }
  assert(flipped_pos.size() == 2*num_flaws_);
  assert(num_flipped_dsteps == num_flipped_usteps);

  // reconstruct x0 by flipping back all previously computed steps
  std::vector<int> x0(x);
  for (int i = 0; i < flipped_pos.size(); ++i) {
    int p = flipped_pos[i];
    x0[p] = 1 - x0[p];
  }

  flip_pos_.resize(length, 0);
  compute_flip_pos(x0);
}

void DyckPath::compute_flip_pos(const std::vector<int> &x0) {
  int length = x0.size();
  assert(length % 2 == 0);

  // array of bidirectional pointers below hills of
  // the Dyck path between pairs of upstep/downstep
  // that see each other on the same height
  std::vector<int> next_step(length, 0);

  // prev_ustep_height[h] contains the index of the last upstep
  // starting at height h that has been encountered when moving
  // along the Dyck path from left to right
  std::vector<int> left_ustep_height(length/2, -1);

  int height = 0;
  for (int i = 0; i < length; ++i) {
    if (x0[i] == 0) {  // downstep
      assert(height >= 1);
      int left = left_ustep_height[height-1];
      assert((left >= 0) && (left < i));
      next_step[left] = i;
      next_step[i] = left;
    } else {  // upstep
      assert(height >= 0);
      left_ustep_height[height] = i;
    }
    height += (2*x0[i] - 1);
  }

  compute_flip_pos_rec(x0, 0, length-1, 0, true, next_step);
}

// For all positions of the Dyck path x in the intervall [l,r]
// (which form one or multiple hills), define the alternating
// permutation at positions [a,a+(r-l)] by filling those positions
// with values from the interval [l,r].
void DyckPath::compute_flip_pos_rec(const std::vector<int> &x0, int l, int r, int a, bool forward, const std::vector<int> &next_step)
{
  if (l > r) {  // base case of the recursion
    return;
  }
  assert((x0[l] == 1) && (x0[r] == 0));
  if (forward) {
    int m = next_step[l];
    assert((m <= r) && (x0[m] == 0));
    flip_pos_[a] = m;
    flip_pos_[a+(m-l)] = l;
    this->compute_flip_pos_rec(x0, l+1, m-1, a+1, false, next_step);
    this->compute_flip_pos_rec(x0, m+1, r, a+(m-l)+1, true, next_step);
  } else {
    int m = next_step[r];
    assert((m >= l) && (x0[m] == 1));
    flip_pos_[a] = m;
    flip_pos_[a+(r-m)] = r;
    this->compute_flip_pos_rec(x0, l, m-1, a+(r-m)+1, false, next_step);
    this->compute_flip_pos_rec(x0, m+1, r-1, a+1, true, next_step);
  }
}

int DyckPath::count_steps(int s, int h) const {
  int count = 0;
  int height = 0;
  for (int i = 0; i < steps_.size(); ++i) {
    if ((steps_[i] == s) && (height == h)) {
      ++count;
    }
    height += (2*steps_[i] - 1);
  }
  return count;
}

int DyckPath::count_usteps(int h) const {
  return this->count_steps(1, h);
}

int DyckPath::count_dsteps(int h) const {
  return this->count_steps(0, h);
}

void DyckPath::apply_f() {
  if (num_flaws_ == steps_.size()/2) {  // maximum flaw value reached, cannot apply f anymore
    return;
  }
  int d = flip_pos_[2*num_flaws_];
  int u = flip_pos_[2*num_flaws_ + 1];
  assert((u < d) && (steps_[u] == 1) && (steps_[d] == 0));
  steps_[u] = 0;
  steps_[d] = 1;
  ++num_flaws_;
}

void DyckPath::apply_finv() {
  if (num_flaws_ == 0) {  // minimum flaw value reached, cannot apply f^{-1} anymore
    return;
  }
  int u = flip_pos_[2*num_flaws_ - 2];
  int d = flip_pos_[2*num_flaws_ - 1];
  assert((u > d) && (steps_[u] == 1) && (steps_[d] == 0));
  steps_[u] = 0;
  steps_[d] = 1;
  --num_flaws_;
}
