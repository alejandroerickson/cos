/*
 * Copyright (c) 2018 Torsten Muetze, Aaron Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <vector>
#include <getopt.h>
// The following algorithms are implemented in Joerg Arndt's FXT library
#include "comb/mset-perm-lex.h"
#include "comb/mset-perm-gray.h"
#include "comb/mset-perm-pref.h" // TODO use other implementation

// display help
void help() {
  std::cout << "./multiperm [options]  generate multiset permutations" << std::endl;
  std::cout << "-h                  display this help" << std::endl;
  std::cout << "-f \"f1 f2 ... fd\"   the element frequencies in the multiset" << std::endl;
  std::cout << "-l{-1,0,1,2,...}    number of permutations to list; -1 for all" << std::endl;
  std::cout << "-a{0,1,...,3}       algorithm *0=lexicographic" << std::endl;
  std::cout << "                               1=minimal-change (Lunnon)" << std::endl;
  std::cout << "                               2=prefix rotations (Williams' cool-lex)" << std::endl;
  std::cout << "                               3=prefix rotations (shorthand universal cycle)" << std::endl;
  std::cout << "examples:  ./multiperm -f \"1 1 3 2\" will generate the permutations of {0,1,2,2,2,3,3}" << std::endl;
  std::cout << "           ./multiperm -f \"3 3\" -a1" << std::endl;
  std::cout << "           ./multiperm -f \"1 1 1 1 1\" -a1 -l20" << std::endl;
}

void opt_f_missing() {
  std::cerr << "option -f is mandatory" << std::endl;
}

// we do not use the FXT print functions, as they are 0-based,
// but rather our own print function, which is 1-based
void print_perm(const ulong *x, int n, int rebase) {
  for (int i = 0; i < n; i++) {
    std::cout << x[i] + rebase << " ";  // rebased output
  }
}

int main(int argc, char *argv[]) {
  bool f_set = false;  // flag whether option -f is present
  int algo = 0;  // use first algorithm by default
  const int num_algos = 4;
  long long limit = -1;  // compute all permutations by default

  std::stringstream stream;  // string stream used for reading the frequency string
  std::string entry_string; // single entry of the stream as a string
  int entry;  // single entry of the stream as an int
  int num_entries;  // the number of entries that can be parsed as an int
  
  unsigned long num_distinct;  // number of distinct elements as required by FXT
  unsigned long *freqs;  // frequencies of each symbol as required by FXT
  int length;  // total length of the multiset permutation
 
  // process command line options
  int c;
  while ((c = getopt(argc, argv, ":hf:l:a:")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
        
      case 'f':
        // Copy the argument into a stream to figure out how many integers it has.
        num_entries = 0;
        stream << optarg;            
        while (!stream.eof()) {
            // Attempt to convert the next string into an integer.
            stream >> entry_string;
            if (std::stringstream(entry_string) >> entry) {
                num_entries++;
            }
            if (entry <= 0) {
              std::cerr << "element frequencies must be positive numbers" << std::endl;
              return 1;
            }
        }

        // Make sure there was actually an entry to describe the multiset.
        if (num_entries < 1) {
          std::cerr << "frequency sequence must be nonempty" << std::endl;
          return 1;
        }
        f_set = true;
        
        // Initialize the frequency data for FXT.
        num_distinct = num_entries;
        freqs = (unsigned long *)malloc(num_entries * sizeof(unsigned long));
        length = 0;
        
        // Reset the stream to be read again.
        stream.str("");
        stream.clear();
        stream << optarg;

        // Fill in the frequency data for FXT.
        num_entries = 0;
        while (!stream.eof()) {
            // Attempt to convert the next string into an integer.
            stream >> entry_string;
            if (std::stringstream(entry_string) >> entry) {
                freqs[num_entries++] = entry;
                length += entry;
            }                           
        }
        
        // Clear the stream to be read again.
        stream.str("");
        stream.clear();
        break;
        
      case 'l':
        limit = atoi(optarg);
        if (limit < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;        
      case 'a':
        algo = atoi(optarg);
        if ((algo < 0) || (algo > num_algos - 1)) {
          std::cerr << "option -a must be followed by an integer from {0,1,...," << num_algos - 1 << "}" << std::endl;
          return 1;
        }
        break;
      case ':':
        std::cerr << "option -" << (char) optopt << " requires an operand" << std::endl;
        return 1;
      case '?':
        std::cerr << "unrecognized option -" << (char) optopt << std::endl;
        return 1;
    }
  }
  if (!f_set) {
    opt_f_missing();
    help();
    return 1;
  }

  // Compute the first limit objects.
  // A value limit < 0 means computing all of them.
  switch (algo) {
    case 0:  // currently reuse
      {
      mset_perm_lex x(freqs, num_distinct); 
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) { 
          std::cout << "output limit reached" << std::endl;
          break;
        }
        print_perm(x.data(), length, 1); 
        std::cout << std::endl;
        ++count;
      } while (x.next() != length);
      break;
      }
    case 1:
      {
      mset_perm_gray x(freqs, num_distinct); 
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) { 
          std::cout << "output limit reached" << std::endl;
          break;
        }
        print_perm(x.data(), length, 0);
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    case 2:  
      {
      mset_perm_pref x(freqs, num_distinct); 
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) { 
          std::cout << "output limit reached" << std::endl;
          break;
        }

        print_perm(x.data(), length, 1);
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    case 3:  
      {
      std::cout << "This algorithm has not yet been implemented" << "\n";
      break;
      }
  }

  return 0;
}
