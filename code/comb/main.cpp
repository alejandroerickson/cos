/*
 * Copyright (c) 2018 Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <getopt.h>
// The following algorithms are implemented in Joerg Arndt's FXT library
#include "comb/comb-print.h"
#include "comb/combination-lex.h"
#include "comb/combination-colex.h"
#include "comb/combination-pref.h"
#include "comb/combination-revdoor.h"
#include "comb/combination-emk.h"
#include "comb/combination-chase.h"

// display help
void help() {
  std::cout << "./comb [options]  generate combinations" << std::endl;
  std::cout << "-h                display this help" << std::endl;
  std::cout << "-n{1,2,...}       size n of the ground set" << std::endl;
  std::cout << "-k{0,1,...,n}     size k of the subsets" << std::endl;
  std::cout << "-l{-1,0,1,2,...}  number of combinations to list; -1 for all" << std::endl;
  std::cout << "-a{0,1,...,5}     algorithm *0=lexicographic" << std::endl;
  std::cout << "                             1=colexicographic" << std::endl;
  std::cout << "                             2=prefix rotation (Ruskey-Williams' cool-lex)" << std::endl;
  std::cout << "                             3=minimal-change order (BRGC-based)" << std::endl;
  std::cout << "                             4=strong minimal-change (Eades-McKay algorithm)" << std::endl;
  std::cout << "                             5=two-close Gray code (Chase's sequence)" << std::endl;
  std::cout << "-p{0,1}           print bitstring representation (0*), set representation (1)" << std::endl;
  std::cout << "examples:  ./comb -n7 -k3" << std::endl;
  std::cout << "           ./comb -n7 -k3 -a2" << std::endl;
  std::cout << "           ./comb -n7 -k3 -a3 -l10" << std::endl;
}

void opt_nk_missing() {
  std::cerr << "options -n and -k are mandatory" << std::endl;
}

// we do not use the FXT print functions, as they are 0-based,
// but rather our own print function, which is 1-based
void print_set(const ulong *x, int k) {
  for (int i = 0; i < k; i++) {
    std::cout << x[i] + 1 << " ";  // 1-based output
  }
}

void print_bitstring(const ulong *x, int n) {
  for (int i = 0; i < n; i++) {
    std::cout << x[i];
  }
}

// print given set as bitstring
void print_set_as_bitstring(const ulong *x, int n, int k) {
  ulong y[n];
  for (int i = 0; i < n; i++) {
    y[i] = 0;
  }
  for (int i = 0; i < k; i++) {
    y[x[i]] = 1;
  }
  print_bitstring(y, n);
}

// print given bitstring as set
void print_bitstring_as_set(const ulong *x, int n) {
  ulong y[n];
  int k = 0;
  for (int i = 0; i < n; i++) {
    if (x[i] == 0) continue;
    y[k] = i;
    k++;    
  }
  print_set(y, k);
}

int main(int argc, char *argv[]) {
  int n, k;
  bool n_set = false;  // flag whether option -n is present
  bool k_set = false;  // flag whether option -k is present
  int algo = 0;  // use first algorithm by default
  const int num_algos = 6;
  long long limit = -1;  // compute all combinations by default
  bool print_sets = false;  // print bitstrings by default
  
  // process command line options
  int c;
  while ((c = getopt(argc, argv, ":hn:k:l:a:p:")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
      case 'n':
        n = atoi(optarg);
        if (n < 1) {
          std::cerr << "option -n must be followed by an integer from {1,2,...}" << std::endl;
          return 1;
        }
        n_set = true;
        break;
      case 'k':
        k = atoi(optarg);
        k_set = true;
        break;
      case 'l':
        limit = atoi(optarg);
        if (limit < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;        
      case 'a':
        algo = atoi(optarg);
        if ((algo < 0) || (algo > num_algos - 1)) {
          std::cerr << "option -a must be followed by an integer from {0,1,...," << num_algos - 1 << "}" << std::endl;
          return 1;
        }
        break;
      case 'p':
        {
        int arg = atoi(optarg);
        if ((arg < 0) || (arg > 1)) {
          std::cerr << "option -p must be followed by 0 or 1" << std::endl;
          return 1;
        }
        print_sets = (bool) arg;
        break;
        }
      case ':':
        std::cerr << "option -" << (char) optopt << " requires an operand" << std::endl;
        return 1;
      case '?':
        std::cerr << "unrecognized option -" << (char) optopt << std::endl;
        return 1;
    }
  }
  if (!n_set || !k_set) {
    opt_nk_missing();
    help();
    return 1;
  }
  if ((k < 0) || (k > n)) {
    std::cerr << "option -k must be followed by an integer from {0,1,...,n}" << std::endl;
    return 1;
  }
  if ((k == 0) && (limit != 0)) {
    limit = 1;  // \binom{n}{0} == 1
  }

  // Compute the first limit combinations of k out of n things.
  // A value limit < 0 means computing all of them (\binom{n}{k} many).
  switch (algo) {
    case 0:
      {
      combination_lex x(n, k);  // in the FXT library, lexicographic ordering is w.r.t. the set representation;
      x.last();                 // for lexicographic ordering w.r.t. the bitstring representation we have to go backwards
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) { 
          std::cout << "output limit reached" << std::endl;
          break;
        }
        if (print_sets) { print_set(x.data(), k); } else { print_set_as_bitstring(x.data(), n, k); }
        std::cout << std::endl;
        ++count;
      } while (x.prev() != k);
      break;
      }
    case 1:
      {
      combination_colex x(n, k);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) { 
          std::cout << "output limit reached" << std::endl;
          break;
        }
        if (print_sets) { print_set(x.data(), k); } else { print_set_as_bitstring(x.data(), n, k); }
        std::cout << std::endl;
        ++count;
      } while (x.next() != k);
      break;
      }
    case 2:
      {
      combination_pref x(n, k);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) { 
          std::cout << "output limit reached" << std::endl;
          break; 
        }
        if (print_sets) { print_bitstring_as_set(x.data(), n); } else { print_bitstring(x.data(), n); }
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    case 3:
      {
      combination_revdoor x(n, k);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) { 
          std::cout << "output limit reached" << std::endl;
          break; 
        }
        if (print_sets) { print_set(x.data(), k); } else { print_set_as_bitstring(x.data(), n, k); }
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }      
    case 4:
      {
      combination_emk x(n, k);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) { 
          std::cout << "output limit reached" << std::endl;
          break; 
        }     
        if (print_sets) { print_set(x.data(), k); } else { print_set_as_bitstring(x.data(), n, k); }
        std::cout << std::endl;
        ++count;
      } while (x.next() != k);
      break;
      } 
    case 5:
      {
      combination_chase x(n, k);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) { 
          std::cout << "output limit reached" << std::endl;
          break; 
        }
        if (print_sets) { print_bitstring_as_set(x.data(), n); } else { print_bitstring(x.data(), n); }
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }       
  }

  return 0;
}


