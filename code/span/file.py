#!/usr/bin/env python

#
#  Copyright (c) 2018 Torsten Muetze
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import re

# Take an edge list as an input string and create the output file 'graph.gb'
# in Standford Graph Base (SGB) format. The checksum at the very end of
# the file is set to 0.
# The edge list is specified as a list of edges separated by semicolon,
# and each edge is a pair of vertices separated by comma. The vertex names
# can be arbitrary strings (except ; and ,). All whitespace from input string
# (in particular, from vertex identifiers) is removed.

if (len(sys.argv) < 3):
    print 'Usage: file.py [edge_list] [filename]'
    sys.exit(1)    

edge_list = str(sys.argv[1])
whitespace = re.compile(r'\s+')
# remove all whitespace from input string
edge_list = re.sub(whitespace, '', edge_list)

if ('"' in edge_list):
    print 'Invalid edge list (no double quotes allowed)';
    sys.exit(1);

# compute edge set E and vertex set V of the graph
Es = edge_list.split(';')
E = []
for es in Es:
    e = es.split(',')
    if (len(e) != 2):
        print 'Invalid edge list (edges must be pairs of vertices)';
        sys.exit(1);
    else:
        E.append(e) 
V = sorted(list(set([v for e in E for v in e])))

# number of vertices n
n = len(V)

# compute adjacency lists (remove duplicate edges)
adj = []
m = 0  # number of edges m
for u in V:
    neighbors = []
    for e in E:
        if (e[0] == u):
            neighbors.append(V.index(e[1]))
        elif (e[1] == u):
            neighbors.append(V.index(e[0]))
    # duplicate edges are ignored            
    neighbors_simple = sorted(list(set(neighbors)))
    adj.append(neighbors_simple)
    m += len(neighbors_simple)
m = m / 2  # counted each edge twice

# compute arc indices incident with each vertex
arcIndices = [[] for i in range(n)]
startOfArc = [0 for i in range(2*m)]
arc = 0
for i in range(n):
    for j in adj[i]:
        if (i > j):
            continue
        arcIndices[i].append(arc)
        arcIndices[j].append(arc+1)
        startOfArc[arc] = i
        startOfArc[arc+1] = j
        arc += 2 

# write output file

# write header
filename = str(sys.argv[2])
file = open(filename,'w') 
file.write('* GraphBase graph (util_types IZZZZZAZZZZZZZ,' + str(n) + 'V,' + str(2*m) + 'A)\n')
file.write('"given",' + str(n) + ',' + str(m) + '\n')

# write vertex section
file.write('* Vertices\n')
for i in range(n):
    file.write('"' + V[i] + '",A' + str(arcIndices[i][0]) + ',' + str(len(adj[i])) + '\n')

# write arc section
file.write('* Arcs\n')
for j in range(2*m):
    i = startOfArc[j]
    p = arcIndices[i].index(j)
    tip = adj[i][p]
    file.write('V' + str(tip) + ',' 
               + ('0' if (p == len(arcIndices[i]) - 1) else ('A' + str(arcIndices[i][p+1]))) 
               + ',1,' 
               + ('0' if (p == 0) else ('A' + str(arcIndices[i][p-1]))) + '\n')

file.write('* Checksum 0')
file.close() 