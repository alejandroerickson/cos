/*
 * Copyright (c) 2018 Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <getopt.h>
#include "sigmatau.hpp"
#include "corbett.hpp"
// The following algorithms are implemented in Joerg Arndt's FXT library
#include "comb/perm-lex.h"
#include "comb/perm-colex.h"
#include "comb/perm-rev.h"
#include "comb/perm-heap.h"
#include "comb/perm-trotter.h"
#include "comb/perm-star.h"
#include "comb/perm-derange.h"
#include "comb/perm-st.h"
#include "comb/perm-st-gray.h"

// display help
void help() {
  std::cout << "./perm [options]  generate permutations" << std::endl;
  std::cout << "-h                display this help" << std::endl;
  std::cout << "-n{1,2,...}       size n of the ground set" << std::endl;
  std::cout << "-l{-1,0,1,2,...}  number of permutations to list; -1 for all" << std::endl;
  std::cout << "-a{0,1,...,10}    algorithm *0=lexicographic" << std::endl;
  std::cout << "                             1=colexicographic" << std::endl;
  std::cout << "                             2=reversing prefixes (Zaks)" << std::endl;
  std::cout << "                             3=minimal-change order (Heap)" << std::endl;
  std::cout << "                             4=adjacent transpositions (Steinhaus-Johnson-Trotter)" << std::endl;
  std::cout << "                             5=star transpositions (Ehrlich)" << std::endl;
  std::cout << "                             6=derangement order" << std::endl;
  std::cout << "                             7=single track" << std::endl;
  std::cout << "                             8=single track Gray code" << std::endl;
  std::cout << "                             9=prefix swaps and rotations (Sawada-Williams)" << std::endl;
  std::cout << "                            10=prefix rotations (Corbett)" << std::endl;
  std::cout << "examples:  ./perm -n5" << std::endl;
  std::cout << "           ./perm -n5 -a1" << std::endl;
  std::cout << "           ./perm -n10 -a1 -l20" << std::endl;
}

void opt_n_missing() {
  std::cerr << "option -n is mandatory" << std::endl;
}

// we do not use the FXT print functions, as they are 0-based,
// but rather our own print function, which is 1-based
void print_perm(const ulong *x, int n) {
  for (int i = 0; i < n; i++) {
    std::cout << x[i] + 1 << " ";  // 1-based output
  }
}

int main(int argc, char *argv[]) {
  int n;
  bool n_set = false;  // flag whether option -n is present
  int algo = 0;  // use first algorithm by default
  const int num_algos = 11;
  long long limit = -1;  // compute all permutations by default

  // process command line options
  int c;
  while ((c = getopt(argc, argv, ":hn:l:a:")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
      case 'n':
        n = atoi(optarg);
        if (n < 1) {
          std::cerr << "option -n must be followed by an integer from {1,2,...}" << std::endl;
          return 1;
        }
        n_set = true;
        break;
      case 'l':
        limit = atoi(optarg);
        if (limit < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;
      case 'a':
        algo = atoi(optarg);
        if ((algo < 0) || (algo > num_algos - 1)) {
          std::cerr << "option -a must be followed by an integer from {0,1,...," << num_algos - 1 << "}" << std::endl;
          return 1;
        }
        break;
      case ':':
        std::cerr << "option -" << (char) optopt << " requires an operand" << std::endl;
        return 1;
      case '?':
        std::cerr << "unrecognized option -" << (char) optopt << std::endl;
        return 1;
    }
  }
  if (!n_set) {
    opt_n_missing();
    help();
    return 1;
  }

  // Compute the first limit permutations of {0,1,...,n-1}.
  // A value limit < 0 means computing all of them (n! many).
  switch (algo) {
    case 0:
      {
      perm_lex x(n);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) {
          std::cout << "output limit reached" << std::endl;
          break;
        }
        print_perm(x.data(), n);
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    case 1:
      {
      perm_colex x(n);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) {
          std::cout << "output limit reached" << std::endl;
          break;
        }
        print_perm(x.data(), n);
        std::cout << std::endl;
        ++count;
      } while (x.next() && (n != 1));  // FXT does not terminate properly for n=1
      break;
      }
    case 2:
      {
      perm_rev x(n);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) {
          std::cout << "output limit reached" << std::endl;
          break;
        }
        print_perm(x.data(), n);
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    case 3:
      {
      perm_heap x(n);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) {
          std::cout << "output limit reached" << std::endl;
          break;
        }
        print_perm(x.data(), n);
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    case 4:
      {
      perm_trotter x(n);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) {
          std::cout << "output limit reached" << std::endl;
          break;
        }
        print_perm(x.data(), n);
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    case 5:
      {
      perm_star x(n);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) {
          std::cout << "output limit reached" << std::endl;
          break;
        }
        print_perm(x.data(), n);
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    case 6:
      {
      perm_derange x(n);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) {
          std::cout << "output limit reached" << std::endl;
          break;
        }
        print_perm(x.data(), n);
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    case 7:
      {
      perm_st x(n);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) {
          std::cout << "output limit reached" << std::endl;
          break;
        }
        print_perm(x.data(), n);
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    case 8:
      {
      perm_st_gray x(n);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) {
          std::cout << "output limit reached" << std::endl;
          break;
        }
        print_perm(x.data(), n);
        std::cout << std::endl;
        ++count;
      } while (x.next() && (n != 1));  // FXT does not terminate properly for n=1
      break;
      }
    case 9:
      {
      SigmaTau x(n);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) {
          std::cout << "output limit reached" << std::endl;
          break;
        }
        x.print();
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    case 10:
      {
      Corbett x(n);
      x.first();
      int count = 0;
      do {
        if ((limit >= 0) && (count >= limit)) {
          std::cout << "output limit reached" << std::endl;
          break;
        }
        x.print();
        std::cout << std::endl;
        ++count;
      } while (x.next());
      break;
      }
    }

  return 0;
}
