/*
 * Copyright (c) 2018 Joe Sawada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//---------------------------------------------------------------------------------------
// GENERATING ROOTED OR FREE PLANE TREES WITH N VERTICES USING A LEVEL SEQUENCE REPRESENTATION
// See: J. Sawada, "Generating rooted and free plane trees", ACM Transactions on
//      Algorithms, Vol. 2, No. 1 (2006) 1-13.
// Programmed by: Joe Sawada -- 2005/2018
// Some minor modifications for usage within the Combinatorial Object Server
// by Torsten Muetze.
//---------------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#define MAX 100
int a[MAX];
int n;
int total=0;
int limit=-1;

//--------------------------------------------------------------------------------
void Print() {
int j;

    if ((limit >= 0) && (total >= limit)) {
        printf("output limit reached\n");
        exit(0); 
    }
    total++;
    for(j=0; j<=n-1; j++) {
        printf("%d ", a[j]);
    }
    printf("\n");
}
//--------------------------------------------------------------------------------
void RootedPlane(int t, int p) {
int j;
    
    if(t>n-1){ if ((n-1)%p == 0) Print(); }
    else {
        a[t] = a[t-p];
        RootedPlane(t+1,p);
        for(j=a[t-p] +1; j <= a[t-1]+1; j++) {
            a[t] = j;
            RootedPlane(t+1,t);
        }
    }
}
//--------------------------------------------------------------------------------
void Unicentroid(int t, int p, int s) {
int j,max;

	if(t>n-1)  { if ((n-1)%p == 0) Print(); }
	else {
		max = a[t-1]+1;
		if (s == (n-1)/2) max = 1;
		for(j=a[t-p]; j <= max; j++) {
			a[t] = j; 
			if (j == a[t-p] && j == 1) Unicentroid(t+1,p,1);
			else if (j == a[t-p]) Unicentroid(t+1,p,s+1);
			else Unicentroid(t+1,t,s+1);
		}
	}
}
//--------------------------------------------------------------------------------
void Bicentroid(int t, int p, int flag) {
int j;
    
	if (t>n-1) Print();
	else if (t <  n/2) {
		for(j=1; j <= a[t-1]+1; j++) {
			a[t] = j; 
			Bicentroid(t+1,t,0);
		}
	}
	else if (t == n/2) {
		a[t] = 1; 
		Bicentroid(t+1,t,0);
	}
	else {
		for (j=a[t-p]+1; j<= a[t-1]+1; j++) {
			a[t] = j;
			if (j==a[t-p]+1 && flag == 0) Bicentroid(t+1,p,0);
			else Bicentroid(t+1,t,1);
		}
	}
}
//--------------------------------------------------------------------------------
void usage() {
    printf("Usage: tree [type] [n] [max] (0<=type<=1, n>=1)\n");
}
//--------------------------------------------------------------------------------
int main(int argc, char **argv) {
int type;
    
    if (argc < 3) {
      usage();
      return 1;
    }
    sscanf(argv[1], "%d", &type);
    sscanf(argv[2], "%d", &n);
    if (argc >= 4) {
      sscanf(argv[3], "%d", &limit);
    }
    if ((type < 0) || (type > 1) || (n < 1)) {
      usage();
      return 1;
    }
  
    a[0] = 0; a[1] = 1;
    if (type == 0) {  // type == 0: rooted plane trees
        RootedPlane(2,1);
    }
    else {  // type == 1: free plane trees
        if (n == 1) Print();
        if (n>2) Unicentroid(2,1,1);
        if (n%2 == 0) Bicentroid(2,1,0);
    }
}