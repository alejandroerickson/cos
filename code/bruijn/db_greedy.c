/*
 * Copyright (c) 2017 Joe Sawada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//---------------------------------------------------------------
// Listings of four k-ary de Bruijn sequences generated via known 
// greedy approaches
//
// Programmed by Joe Sawada, 2017.
// Some minor modifications for usage within the Combinatorial
// Object Server by Torsten Muetze.
//----------------------------------------------------------------

#include<stdio.h>
#include<stdlib.h>
#define MAX 40

int n,k,a[MAX+1];
long long int power[MAX], total;

//------------------------------------------------------
void Print(int v) {
    printf("%d", v);
    total++;
}
//------------------------------------------------------
long long int GetNum() {
    long long int sum=0,i;
    
    for (i=1; i<=n; i++) sum += power[i-1] * a[i];
    return sum;
}
//------------------------------------------------------
int Visit(long long int v, char *visited) {
    long long int dec;

    a[n] = v;
    dec = GetNum();
    if (visited[dec] == 0) {
        visited[dec] = 1;
        Print(v);
        return 1;
    }
    return 0;
}
//------------------------------------------------------
// Greedily add the LARGEST value possible
//------------------------------------------------------
void GreedyMax(char *visited) {
    int i;
    
    // Seed string (very important)
    for (i=1; i<=n; i++) a[i] = 0;
    
    while (1) {
        for (i=1; i<n; i++) a[i] = a[i+1];
        for (i=k-1; i>=0; i--) if (Visit(i,visited)) break;
        if  (i < 0) return;
    }
}
//------------------------------------------------------
// Greedily add the SMALLEST value possible
//------------------------------------------------------
void GreedyMin(char *visited) {
    int i;
    
    // Seed string (very important)
    for (i=1; i<=n; i++) a[i] = k-1;
    
    while (1) {
        for (i=1; i<n; i++) a[i] = a[i+1];
        for (i=0; i<=k-1; i++) if (Visit(i, visited)) break;
        if  (i == k) return;
    }
}
//---------------------------------------------------------------
// Greedily try the SAME as the last bit then increment (mod k)
//---------------------------------------------------------------
void GreedySame(char *visited) {
    int i,last;
    
    // Seed string of sequence (very important). When k=3 seed is ...210210210
    a[n] = 0;
    for (i=n-1; i>=1; i--) a[i] = (a[i+1]+1)%k;
    
    // The last bit of the seed starts the sequence so it can start 0^n
    Visit(a[n], visited);
    
    while (1) {
        last = a[n];
        for (i=1; i<n; i++) a[i] = a[i+1];
        for(i=0; i<=k-1; i++) if (Visit((last + i) % k, visited)) break;
        if (i == k) return;
    }
}
//------------------------------------------------------
// Greedily increment the value of the last bit (mod k)
// When binary, this is prefer OPPOSITE
//------------------------------------------------------
void GreedyOpposite(char *visited) {
    int i,j,v,last;
    long long int end, dec;  // End flag and decimal value of string

    // Special case
    if (n == 1) {
      for (i=0; i<k; i++) Print(i);
      return;
    }
  
    // End of sequence flag string
    for (i=1; i<n; i++) a[i] = k-1;
    a[n] = k-2;
    end = GetNum();
  
    // Initial string of sequence (very important)
    for (i=1; i<=n; i++) { a[i] = 0; Print(0); }
    visited[GetNum()] = 1;
    
    while (1) {
        last = a[n];
        for (i=1; i<n; i++) a[i] = a[i+1];
        for(i=1; i<=k; i++){
            v = (last + i) % k;
            a[n] = v;
            dec = GetNum();
            if (visited[dec] == 0) {
                visited[dec] = 1;
                
                // Special case for end of sequence
                if (dec == end) {
                    Print(k-1);
                    for (i=k-2; i>0; i--) {
                        for (j=1; j<=n; j++) Print(i);
                    }
                    return;
                }
                Print(v);
                break;
           }
        }
    }
}
//------------------------------------------------------
void usage() {
    printf("Usage: db2 [k] [n] [rule] (k>=2, n>=1, 1<=rule<=4)\n");
}
//------------------------------------------------------
int main(int argc, char **argv) {
    int i,rule;
    char *visited;
  
    if (argc < 4) {
      usage();
      return 1;
    }
    sscanf(argv[1], "%d", &k);
    sscanf(argv[2], "%d", &n);
    sscanf(argv[3], "%d", &rule);  
    
    if ((rule < 1) || (rule > 4) || (n < 1) || (k < 2)) {
      usage();
      return 1;
    }
  
    // Initialize visited strings to 0
    power[0] = 1;
    for (i=1; i<=n; i++) power[i] = k * power[i-1];
    visited = (char *) malloc(sizeof(char) * power[n]);
    for (i=0; i<power[n]; i++) visited[i] = 0;
    
    total = 0;
    if (rule == 1) GreedyMax(visited);
    if (rule == 2) GreedyMin(visited);
    if (rule == 3) GreedyOpposite(visited);
    if (rule == 4) GreedySame(visited);
    printf("\n");
}
