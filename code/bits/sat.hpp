/*
 * Copyright (c) 2016 Petr Gregor, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SAT_HPP
#define SAT_HPP

#include <vector>
#include "cycle.hpp"

// whether cycle in two consecutive cube levels is to be traversed in backward direction
// (from a_{n,k} to b_{n,k}) or in forward direction (from b_{n,k} to a_{n,k})
enum direction_t {
  backward, forward
};

// whether a stack item is a flip-item or a rec-item
enum rec_step_type_t {
  flip, recurse
};

// class that represents a stack item

class SatStackItem {
public:
  // constructor for flip-items
  explicit SatStackItem(int flip_pos) : flip_pos_(flip_pos) { type_ = flip; };
  // constructor for rec-items
  explicit SatStackItem(int rec_n, int rec_k, direction_t dir) : rec_n_(rec_n), rec_k_(rec_k), dir_(dir) { type_ = recurse; };

  rec_step_type_t type_;
  int flip_pos_;  // bit position to flip
  int rec_n_;  // recurse to dimension n
  int rec_k_;  // recurse to level [k,k+1] of Q_n
  direction_t dir_;  // traverse subpath in backward or forward direction
};

// class that represents a saturating cycle

class SatCycle : public AbstractCycle<std::vector<int> > {
public:
  // Compute a saturated cycle in the cube of dimension n between levels k and l.
  explicit SatCycle(int n, int k, int l, long long limit, visit_f_t visit_f);
  const std::vector<int>& get_first_vertex() const { return x0_; }

  void visit_f_visit(int j) {
    int i = pi_[j];
    assert((i >= 0) && (i <= n_ - 1));
    x_[i] = 1 - x_[i];
    #ifndef NVISIT
    std::vector<int> pos(1, i);
    visit_f_(x_, pos);
    #endif
  }

  void visit_f_log(int j) {
    int i = pi_[j];
    assert((i >= 0) && (i <= n_ - 1));
    flip_seq_.push_back(i);
  }

private:
  std::vector<int> pi_;  // coordinate permutation
  std::vector<int> flip_seq_;  // for logging a bitflip sequence

  // #### auxiliary functions ####

  // Compute a saturated cycle for an even number of levels entirely above or
  // entirely below the middle level.
  void sat_even(int k, int l, bool complement);

  // Compute a saturated cycle between levels k and k+1 below the middle level.
  // If pi == id and dir == forward, then the first three vertices of the cycle
  // are the vertices a_{n,k}, a_{n,k+1} and b_{n,k}.
  // If pi == id and dir == backward, then the first vertex of the cycle is a_{n,k},
  // and the last two vertices are b_{n,k} and a_{n,k+1}.
  // Moreover, the cycle does not visit the vertex b_{n,k+1} at all (unless k = (n-1)/2).
  // Otherwise the cycle is permuted according to pi=(pi(1),...,pi(n)).
  // If visit == false, then no vertices are actually visited along the cycle, but
  // only the bitflip sequence is recorded.
  // The first skip_start many vertices encountered along the cycle are neither
  // visited nor recorded.
  // Returns true if the maximum number of visited vertices is reached
  // (break the surrounding loops).
  // This is the algorithm SatCycle() presented in the paper (involving a stack).
  bool sat_2(int k, direction_t dir, long long limit, long long skip_start, bool visit, const std::vector<int>& pi);

  // Flip the given bit in x_ (possibly call the visit function)
  // and update flip_seq_ and length_.
  // Returns true if the maximum number of visited vertices is reached.
  // If visit == false, then the bitflip position is flipped in x_,
  // but length_ is not modified.
  bool flip_and_visit(int i, bool visit);

  // compute permutations that map the 2-path
  // (c_{2k+1,k}, d_{2k+1,k+1}, e_{2k+1,k}) = (1^{k}0^{k+1}, 1^{k}0^{k-1}10, 01^{k-1}0^{k-1}10)
  // onto
  // (a_{2k+1,k}, a_{2k+1,k+1}, b_{2k+1,k}) = (0^{k+1}1^{k}, 0^{k}1^{k+1}, 0^{k}1^{k}0)
  // or (b_{2k+1,k}, a_{2k+1,k+1}, a_{2k+1,k}), respectively
  void map_cde_onto_aab(int k, std::vector<int>& pi);
  void map_cde_onto_baa(int k, std::vector<int>& pi);

  // compute permutation that maps the first four vertices of a saturated cycle
  // a_{n,k}, a_{n,k+1}, b_{n,k}, x onto a_{n,k}, a_{n,k+1}, b_{n,k} and b_{n,k+1}
  void cycle_permutation(int k, std::vector<int>& pi);
};

#endif
