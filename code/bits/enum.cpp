/*
 * Copyright (c) 2016 Petr Gregor, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cassert>
#include <stack>
#include <vector>
#include "enum.hpp"
#include "math.hpp"
#include "mlc_hamcycle.hpp"
#include "trim.hpp"

TightEnum::TightEnum(int n, int k, int l, long long limit, visit_f_t visit_f) :
  AbstractCycle(n, limit, visit_f) {
  assert((n >= 2) && (k >= 0) && (l > k) && (l <= n));  // 0 <= k < l <= n

  if ((k == 0) || (l == n) || ((l - k) % 2 == 0)) {  // odd number of levels k,k+1,...,l
    // in this case we compute a saturated cycle by trimming a Gray code to levels k,k+1,...,l
    long long limitp = num_vertices_Qnkl(n, k, l);
    if (limit_ >= 0) {
      limitp = std::min(limitp, limit_);
    }
    TrimmedGrayCode gc(n, k - 1, l + 1, false, limitp, visit_f);
    const std::vector<int>& x0 = gc.get_first_vertex();
    x0_.insert(x0_.end(), x0.begin(), x0.end());
    length_ = gc.get_length();
  } else {  // even number of levels k,k+1,...,l
    assert((k >= 1) && (l <= n-1));
    assert((l <= (n + 1)/2) || (k >= n/2));
    if (l <= (n+1)/2) {  // all levels lie below the middle level
      enum_even(k, l, false);
    } else {  // all levels lie above the middle level
      enum_even(n - l, n - k, true);
    }
    // once the generalized middle levels conjecture is proved and
    // an algorithm for it is available, then it can be included here ;)
  }
}

void TightEnum::enum_even(int k, int l, bool complement) {
  assert((k >= 1) && ((l - k) % 2 == 1) && (l <= (n_ + 1)/2));

  // bitflip sequence for complementary traversal is exactly the same,
  // only the roles of 0's and 1's are interchanged
  int zero_bit = complement ? 1 : 0;
  int one_bit = complement ? 0 : 1;

  x0_.resize(n_, zero_bit);
  if (l - k == 1) {  // two consecutive levels (including middle levels)
    // initialize starting and current vertex to a_{n,k} = 0^{n-k}1^k
    for (int i = 1; i <= k; ++i) {
      x0_[n_ - i] = one_bit;
    }
    assert(is_a_vertex(x0_, complement, n_, k));
  } else {
    // initialize starting and current vertex to b_{n,k} = 0^{n-k-1}1^k0
    for (int i = 1; i <= k; ++i) {
      x0_[n_ - i - 1] = one_bit;
    }
    assert(is_b_vertex(x0_, complement, n_, k));
  }
  x_ = x0_;

  length_ = 0;
  if (length_ == limit_) {  // compute no vertices at all
    return;
  }

  long long limit = binom(n_, k) + binom(n_, k + 1);  // number of vertices to be visited in levels [k,k+1]
  std::vector<int> id;
  if (l - k == 1) {  // two consecutive levels (including middle levels)
    // the cycle between levels k and k+1 must start with the 3-path (a_{n,k}, a_{n,k+1}, b_{n,k}, b_{n,k+1})
    // and visits all vertices in these two levels
    if (enum_2(cycle_3path, k, forward, limit, 0)) {  // visit all vertices
      return;
    }
    assert(is_a_vertex(x_, complement, n_, k));
    return;
  }
  assert(l - k >= 3);  // at least four consecutive levels
  // the cycle between levels k and k+1 must contain the switched 2-path (b_{n,k}, b_{n,k+1}, a_{n,k+1}),
  // it starts at b_{n,k}, ends at a_{n,k+1} and visits all vertices in these two levels except b_{n,k+1}
  if (enum_2(cycle_sw2path, k, backward, limit - 2, 0)) {  // visit all except last two vertices b_{n,k+1} and b_{n,k}
    return;
  }
  assert(is_a_vertex(x_, complement, n_, k + 1));

  int i;
  std::vector<int> pos;

  // move up from level k to level l traversing every second cycle
  int jmax = (l - k - 1) / 2;
  for (int j = 1; j <= jmax; ++j) {
    int kp = k + 2*j;  // k'
    assert(kp <= (n_ + 1)/2);
    long long limit = binom(n_, kp) + binom(n_, kp + 1);  // number of vertices to be visited in levels [k',k'+1]
    if ((j % 2) == 1) {
      // move up from a_{n,k'-1} to a_{n,k'}
      assert(is_a_vertex(x_, complement, n_, kp - 1));
      i = n_ - (kp - 1) - 1;
      assert(x_[i] == zero_bit);
      pos.clear();
      pos.push_back(i);
      if (flip_and_visit(pos)) {
        return;
      }
      assert(is_a_vertex(x_, complement, n_, kp));
      // visit all vertices from a_{n,k'} to b_{n,k'+1} except a_{n,k'+1} and b_{n,k'}
      if (enum_2(cycle_3path, kp, backward, limit - 3, 0)) {
        return;
      }
      assert(is_b_vertex(x_, complement, n_, kp + 1));
      if (j == jmax) {  // uppermost cycle
        // move along a distance-2 step from b_{n,k'+1} to a_{n,k'+1}
        pos.clear();
        pos.push_back(n_ - kp - 2);
        pos.push_back(n_ - 1);
        if (flip_and_visit(pos)) {
          return;
        }
        assert(is_a_vertex(x_, complement, n_, kp + 1));
        // move from a_{n,k'+1} to b_{n,k'}
        pos.clear();
        pos.push_back(n_ - 1);
        if (flip_and_visit(pos)) {
          return;
        }
        assert(is_b_vertex(x_, complement, n_, kp));
      }
    } else {  // (j % 2) == 0
      // move up from b_{n,k'-1} to b_{n,k'}
      assert(is_b_vertex(x_, complement, n_, kp - 1));
      i = n_ - (kp - 1) - 2;
      assert(x_[i] == zero_bit);
      pos.clear();
      pos.push_back(i);
      if (flip_and_visit(pos)) {
        return;
      }
      assert(is_b_vertex(x_, complement, n_, kp));
      if (j < jmax) {
        // move up from b_{n,k'} to a_{n,k'+1}
        i = n_ - 1;
        assert(x_[i] == zero_bit);
        pos.clear();
        pos.push_back(i);
        if (flip_and_visit(pos)) {
          return;
        }
        assert(is_a_vertex(x_, complement, n_, kp + 1));
      } else {
        assert(kp == l - 1);
        // in the uppermost cycle visit all vertices from b_{n,l} to a_{n,l}
        // move from b_{n,k'} to a_{n,k'+1}
        pos.clear();
        pos.push_back(n_ - 1);
        if (flip_and_visit(pos)) {
          return;
        }
        assert(is_a_vertex(x_, complement, n_, kp + 1));
        // move along a distance-2 step from a_{n,k'+1} to b_{n,k'+1}
        pos.clear();
        pos.push_back(n_ - kp - 2);
        pos.push_back(n_ - 1);
        if (flip_and_visit(pos)) {
          return;
        }
        assert(is_b_vertex(x_, complement, n_, kp + 1));
        // visit remaining vertices
        if (enum_2(cycle_3path, kp, forward, limit, 3)) {
          return;
        }
        assert(is_a_vertex(x_, complement, n_, kp));
      }
    }
  }

  // move down from level l to level k traversing every other second cycle
  for (int j = (l - k - 1)/2 - 1; j >= 1; --j) {
    int kp = k + 2*j;
    if ((j % 2) == 0) {
      // move down from b_{n,k'+2} to b_{n,k'+1}
      assert(is_b_vertex(x_, complement, n_, kp + 2));
      i = n_ - (kp + 1) - 2;
      assert(x_[i] == one_bit);
      pos.clear();
      pos.push_back(i);
      if (flip_and_visit(pos)) {
        return;
      }
      assert(is_b_vertex(x_, complement, n_, kp + 1));
      long long limit = binom(n_, kp) + binom(n_, kp + 1);  // number of vertices to be visited in levels [k',k'+1]
      // visit all vertices from b_{n,k'+1} to a_{n,k'} except a_{n,k'+1} and b_{n,k'}
      if (enum_2(cycle_3path, kp, forward, limit, 3)) {
        return;
      }
      assert(is_a_vertex(x_, complement, n_, kp));
    } else {  // (j % 2) == 1
      // move down from a_{n,k'+2} to a_{n,k'+1}
      assert(is_a_vertex(x_, complement, n_, kp + 2));
      i = n_ - (kp + 1) - 1;
      assert(x_[i] == one_bit);
      pos.clear();
      pos.push_back(i);
      if (flip_and_visit(pos)) {
        return;
      }
      assert(is_a_vertex(x_, complement, n_, kp + 1));
      // move down from a_{n,k'+1} to b_{n,k'}
      i = n_ - 1;
      assert(x_[i] == one_bit);
      pos.clear();
      pos.push_back(i);
      if (flip_and_visit(pos)) {
        return;
      }
      assert(is_b_vertex(x_, complement, n_, kp));
    }
  }

  // move down from b_{n,k+2} to b_{n,k+1}
  i = n_ - (k + 1) - 2;
  assert(x_[i] == one_bit);
  pos.clear();
  pos.push_back(i);
  if (flip_and_visit(pos)) {
    return;
  }
  assert(is_b_vertex(x_, complement, n_, k + 1));

  // move down from b_{n,k+1} to starting vertex b_{n,k}
  i = n_ - k - 2;
  assert(x_[i] == one_bit);
  pos.clear();
  pos.push_back(i);
  if (flip_and_visit(pos)) {
    return;
  }
  assert(is_b_vertex(x_, complement, n_, k));
}

bool TightEnum::enum_2(cycle_type_t type, int k, direction_t dir, long long limit, long long skip_start) {
  // local variable that counts the number of vertices visited
  // along this cycle in the cube of dimension n_ in levels [k,k+1]
  long long length = 0;

  // There is no local current vertex we need to keep track of.
  // All transformations are done directly on the vertex x_
  // (according to the permutation pi).

  std::stack<EnumStackItem> s;

  if (type == cycle_3path) {
    if (dir == forward) {
      s.push(EnumStackItem(cycle_3path, n_, k, forward, 0, 0));  // (4) visit all vertices from b_{n,k+1} to a_{n,k} except a_{n,k+1} and b_{n,k}
      s.push(EnumStackItem(std::vector<int> (1, n_ - k - 2)));   // (3) b_{n,k} --> b_{n,k+1}
      s.push(EnumStackItem(std::vector<int> (1, n_ - 1)));       // (2) a_{n,k+1} --> b_{n,k}
      s.push(EnumStackItem(std::vector<int> (1, n_ - k - 1)));   // (1) a_{n,k} --> a_{n,k+1}
    } else {
      assert(dir == backward);
      s.push(EnumStackItem(std::vector<int> (1, n_ - k - 1)));    // (4) a_{n,k+1} --> a_{n,k}
      s.push(EnumStackItem(std::vector<int> (1, n_ - 1)));        // (3) b_{n,k} --> a_{n,k+1}
      s.push(EnumStackItem(std::vector<int> (1, n_ - k - 2)));    // (2) b_{n,k+1} --> b_{n,k}
      s.push(EnumStackItem(cycle_3path, n_, k, backward, 0, 0));  // (1) visit all vertices from a_{n,k} to b_{n,k+1} except a_{n,k+1} and b_{n,k}
    }
  } else {
    assert(type == cycle_sw2path);
    if (dir == forward) {
      s.push(EnumStackItem(cycle_sw2path, n_, k, forward, 0, 0));  // (3) visit all vertices from a_{n,k+1} to b_{n,k} except b_{n,k+1}
      std::vector<int> pos;
      pos.push_back(n_ - k - 2);  // 1-bit to toggle
      pos.push_back(n_ - 1);      // 0-bit to toggle
      s.push(EnumStackItem(pos));                                  // (2) b_{n,k+1} --> a_{n,k+1}
      s.push(EnumStackItem(std::vector<int> (1, n_ - k - 2)));     // (1) b_{n,k} --> b_{n,k+1}
    } else {
      assert(dir == backward);
      s.push(EnumStackItem(std::vector<int> (1, n_ - k - 2)));      // (3) b_{n,k+1} --> b_{n,k}
      std::vector<int> pos;
      pos.push_back(n_ - k - 2);  // 0-bit to toggle
      pos.push_back(n_ - 1);      // 1-bit to toggle
      s.push(EnumStackItem(pos));                                   // (2) a_{n,k+1} --> b_{n,k+1}
      s.push(EnumStackItem(cycle_sw2path, n_, k, backward, 0, 0));  // (1) visit all vertices from b_{n,k} to a_{n,k+1} except b_{n,k+1}
    }
  }

  while (!s.empty()) {
    EnumStackItem it = s.top();
    s.pop();
    if (it.type_ == flip) {  // popped a flip-item, perform bitflip(s)
      if (skip_start > 0) {
        --skip_start;
      } else {
        if (flip_and_visit(it.flip_pos_)) {
          return true;
        }
        ++length;
        if (length == limit) {
          return false;
        }
      }
    } else {  // popped a rec-item, perform one recursion step
      assert(it.type_ == recurse);
      int cycle_type = it.cycle_type_;
      int n = it.rec_n_;
      int k = it.rec_k_;
      int dir = it.dir_;
      int last_rot_size = it.last_rot_size_;
      int num_rots = it.num_rots_;
      if ((k >= 1) && (n == 2*k + 1)) {  // middle levels conjecture cycle
        assert(cycle_type == cycle_3path);
        long long mlc_length = num_vertices_Qnkl(n, k, k + 1);
        long long limitp = std::min(mlc_length, limit - length + 3);  // first 3 vertices will be skipped
        if (limit_ >= 0) {
           limitp = std::min(limitp, limit_ - length_ + 3);   // first 3 vertices will be skipped
        }
        if (dir == forward) {
          std::vector<int> pi_mlc, pi_rot;
          map_cdef_onto_aabb(k, pi_mlc);
          rot_perm(n, num_rots, last_rot_size, pi_rot);
          product_permutation(pi_rot, pi_mlc, pi_);
          // Visit all vertices from b_{n,k+1} to a_{n,k} except a_{n,k+1} and b_{n,k}
          // (this is achieved by skipping the first 3 vertices).
          HamCycle hc(k, limitp, 3, this, true);
          length_ += limitp - 3;
          if ((limit_ >= 0) && (length_ == limit_)) {
            return true;
          }
          length += limitp - 3;
          if (length == limit) {
            return false;
          }
        } else {
          assert(dir == backward);
          std::vector<int> pi_mlc, pi_rot;
          map_cdef_onto_bbaa(k, pi_mlc);
          rot_perm(n, num_rots, last_rot_size, pi_rot);
          product_permutation(pi_rot, pi_mlc, pi_);
          // Visit all vertices from b_{n,k+1} to a_{n,k} except a_{n,k+1} and b_{n,k}
          // (this is achieved by skipping the first 3 vertices).
          HamCycle hc(k, limitp, 3, this, true);
          length_ += limitp - 3;
          if ((limit_ >= 0) && (length_ == limit_)) {
            return true;
          }
          length  += limitp - 3;
          if (length == limit) {
            return false;
          }
        }
      } else if ((k >= 1) && (n > 2*k + 1)) {  // recursion step
        if (cycle_type == cycle_3path) {
          if (dir == forward) {
            // visit all vertices from b_{n,k+1} to a_{n,k} except a_{n,k+1} and b_{n,k}
            // push actions onto the stack in reverse order
            s.push(EnumStackItem(cycle_sw2path, n - 1, k - 1, forward, num_rots + 1, n - 1));  // (4) (a_{n-1,k},1) -- > (b_{n-1,k-1},1)
            std::vector<int> pos;
            pos.push_back(rot(n - k - 2, num_rots + 1, n - 1));  // 1-bit to toggle
            pos.push_back(rot(n - 1, num_rots, last_rot_size));  // 0-bit to toggle
            s.push(EnumStackItem(pos));                                                        // (3) (a_{n-1,k+1},0) --> (a_{n-1,k},1)
            s.push(EnumStackItem(std::vector<int> (1, rot(n - k - 2, num_rots + 1, n - 1))));  // (2) (a_{n-1,k},0) --> (a_{n-1,k+1},0)
            s.push(EnumStackItem(cycle_3path, n - 1, k, forward, num_rots + 1, n - 1));        // (1) (b_{n-1,k+1},0) --> (a_{n-1,k},0)
          } else {
            assert(dir == backward);
            // visit all vertices from a_{n,k} to b_{n,k+1} except a_{n,k+1} and b_{n,k}
            // push actions onto the stack in reverse order
            s.push(EnumStackItem(cycle_3path, n - 1, k, backward, num_rots + 1, n - 1));        // (4) (a_{n-1,k},0) --> (b_{n-1,k+1},0)
            s.push(EnumStackItem(std::vector<int> (1, rot(n - k - 2, num_rots + 1, n - 1))));  // (3) (a_{n-1,k+1},0) --> (a_{n-1,k},0)
            std::vector<int> pos;
            pos.push_back(rot(n - k - 2, num_rots + 1, n - 1));  // 0-bit to toggle
            pos.push_back(rot(n - 1, num_rots, last_rot_size));  // 1-bit to toggle
            s.push(EnumStackItem(pos));                                                        // (2) (a_{n-1,k},1) --> (a_{n-1,k+1},0)
            s.push(EnumStackItem(cycle_sw2path, n - 1, k - 1, backward, num_rots + 1, n - 1));  // (1) (b_{n-1,k-1},1) -- > (a_{n-1,k},1)
          }
        } else {
          assert(cycle_type == cycle_sw2path);
          if (dir == forward) {
            // visit all vertices from a_{n,k+1} to b_{n,k} except b_{n,k+1}
            // push actions onto the stack in reverse order
            s.push(EnumStackItem(cycle_3path, n - 1, k, forward, num_rots, last_rot_size));        // (5) (b_{n-1,k+1},0) --> (a_{n-1,k},0) == b_{n,k}
            s.push(EnumStackItem(std::vector<int> (1, rot(n - k - 3, num_rots, last_rot_size))));  // (4) (b_{n-1,k},0) --> (b_{n-1,k+1},0)
            s.push(EnumStackItem(std::vector<int> (1, rot(n - 1, num_rots, last_rot_size))));      // (3) (b_{n-1,k},1) --> (b_{n-1,k},0)
            s.push(EnumStackItem(std::vector<int> (1, rot(n - k - 2, num_rots, last_rot_size))));  // (2) (b_{n-1,k-1},1) --> (b_{n-1,k},1)
            s.push(EnumStackItem(cycle_sw2path, n - 1, k - 1, forward, num_rots, last_rot_size));  // (1) (a_{n-1,k},1) == a_{n,k+1} --> (b_{n-1,k-1},1)
          } else {
            assert(dir == backward);
            // visit all vertices from b_{n,k} to a_{n,k+1} except b_{n,k+1}
            // push actions onto the stack in reverse order
            s.push(EnumStackItem(cycle_sw2path, n - 1, k - 1, backward, num_rots, last_rot_size));  // (5) (b_{n-1,k-1},1) --> (a_{n-1,k},1) == a_{n,k+1}
            s.push(EnumStackItem(std::vector<int> (1, rot(n - k - 2, num_rots, last_rot_size))));   // (4) (b_{n-1,k},1) --> (b_{n-1,k-1},1)
            s.push(EnumStackItem(std::vector<int> (1, rot(n - 1, num_rots, last_rot_size))));       // (3) (b_{n-1,k},0) --> (b_{n-1,k},1)
            s.push(EnumStackItem(std::vector<int> (1, rot(n - k - 3, num_rots, last_rot_size))));   // (2) (b_{n-1,k+1},0) --> (b_{n-1,k},0)
            s.push(EnumStackItem(cycle_3path, n - 1, k, backward, num_rots, last_rot_size));        // (1) (a_{n-1,k},0) == b_{n,k} --> (b_{n-1,k+1},0)
          }
        }
      } else {
        // base case of the induction
        assert((k == 0) && (cycle_type == cycle_sw2path));
        if (dir == forward) {
          // visit all vertices from a_{n,1} to b_{n,0} except b_{n,1}
          // push actions onto the stack in reverse order
          s.push(EnumStackItem(std::vector<int> (1, rot(0, num_rots, last_rot_size))));  // (2) 10^{n-1} --> 0^n == b_{n,0}
          for (int i = 0; i <= n - 3; ++i) {                                             // (1) a_{n,1} --> 10^{n-1} (visit all vertices on level 1 except b_{n,1})
            std::vector<int> pos;
            pos.push_back(rot((i == n - 3) ? n - 1 : i + 1, num_rots, last_rot_size));  // 0-bit to toggle
            pos.push_back(rot(i, num_rots, last_rot_size));                             // 1-bit to toggle
            s.push(EnumStackItem(pos));
          }
        } else {
          assert(dir == backward);
          // visit all vertices from b_{n,0} to a_{n,1} except b_{n,1}
          // push actions onto the stack in reverse order
          for (int i = n - 3; i >= 0; --i) {                                             // (2) 10^{n-1} --> a_{n,1} (visit all vertices on level 1 except b_{n,1})
            std::vector<int> pos;
            pos.push_back(rot((i == n - 3) ? n - 1 : i + 1, num_rots, last_rot_size));  // 1-bit to toggle
            pos.push_back(rot(i, num_rots, last_rot_size));                             // 0-bit to toggle
            s.push(EnumStackItem(pos));
          }
          s.push(EnumStackItem(std::vector<int> (1, rot(0, num_rots, last_rot_size))));  // (1) 0^n == b_{n,0} --> 10^{n-1}
        }
      }
    }
  }
  return false;  // stack is empty
}

bool TightEnum::flip_and_visit(std::vector<int>& pos) {
  assert((pos.size() == 1) || (pos.size() == 2));  // should flip one or two bits
  std::sort(pos.begin(), pos.end());  // want flipped coordinate positions to appear in increasing order
  for (int j = 0; j < pos.size(); ++j) {
    int i = pos[j];
    assert((0 <= i) && (i <= n_ - 1));
    x_[i] = 1 - x_[i];
  }
  #ifndef NVISIT
  visit_f_(x_, pos);
  #endif
  ++length_;
  if ((limit_ >= 0) && (length_ == limit_)) {
    return true;
  }
  return false;
}

void TightEnum::map_cdef_onto_aabb(int k, std::vector<int>& pi) {
  pi.resize(2*k + 1);
  if (k == 1) {
    pi[0] = 2;
    pi[1] = 1;
    pi[2] = 0;
  } else {
    pi[0] = 2*k;
    for (int i = 1; i <= k - 1; ++i) {
      pi[i] = k + i;
    }
    for (int i = k; i <= 2*k - 3; ++i) {
      pi[i] = i - k;
    }
    pi[2*k - 2] = k - 1;
    pi[2*k - 1] = k;
    pi[2*k]     = k - 2;
  }
}

void TightEnum::map_cdef_onto_bbaa(int k, std::vector<int>& pi) {
  pi.resize(2*k + 1);
  if (k == 1) {
    pi[0] = 2;
    pi[1] = 0;
    pi[2] = 1;
  } else {
    pi[0] = 2*k;
    for (int i = 1; i <= k - 1; ++i) {
      pi[i] = i - 1;
    }
    for (int i = k; i <= 2*k - 3; ++i) {
      pi[i] = i + 1;
    }
    pi[2*k - 2] = k;
    pi[2*k - 1] = k - 1;
    pi[2*k]     = 2*k - 1;
  }
}

int TightEnum::rot(int i, int num_rots, int last_rot_size) {
  assert(i >= 0);
  if (num_rots == 0) {
    return i;
  }
  if (i <= last_rot_size - 2) {
    return i + num_rots;
  }
  return num_rots - 1;
}

void TightEnum::rot_perm(int n, int num_rots, int last_rot_size, std::vector<int>& pi) {
  pi.resize(n);
  for (int i = 0; i < n; ++i) {
    pi[i] = rot(i, num_rots, last_rot_size);
  }
}
