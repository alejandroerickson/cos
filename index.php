<!DOCTYPE html>
<html>
<?php include "head.html"; ?>

<body>

<nav id="nav0" class="indx"></nav>

<div id="main">

<h1>The Combinatorial Object Server++</h1>
Welcome! On this website you can
<ul>
<li>generate combinatorial objects with parameters of your choice</li>
<li>explore various combinatorial algorithms</li>
<li>download free source code for those algorithms (GNU General Public License)</li>
</ul>
Have fun exploring!<br>
<br>
The Combinatorial Object Server was first launched by <a href="http://www.cs.uvic.ca/~ruskey/">Frank Ruskey</a> at the University of Victoria (Canada).
The original website has been shut down for some time, and this new version has been relaunched in 2018 by <a href="http://tmuetze.de">Torsten  M&uuml;tze</a>, <a href="http://www.socs.uoguelph.ca/~sawada/">Joe Sawada</a> and <a href="https://simons-rock.edu/academics/faculty-bios/science-mathematics-and-computing-faculty/aaron-williams.php">Aaron Williams</a>.
We plan to bring the original Combinatorial Object Server content back online step by step, and to extend it by new material.
This website also provides an experimental interface to many of the combinatorial algorithms that are part of the <a href="https://www.jjj.de/fxt/">FXT library</a> written in C++ by <a href="https://www.jjj.de/">J&ouml;rg Arndt</a>.<br>
<br>
This website is intended as a community project, and welcomes your contributions and feedback (new algorithms/code/references)!
All code is available via the public GIT repository <a href="https://gitlab.com/tmuetze81/cos">https://gitlab.com/tmuetze81/cos</a>.
Please also let us know when you find the material on this website useful for your research.<br>

<!--
<svg width="45" height="35">
<style> .logo { font: bold 20px sans-serif; pointer-events: none; } </style>
<text x="0" y="20" class="logo">COS</text>
<text x="10.5" y="33" class="logo">++</text>
</svg>
-->
<img height="60" src="img/wizard.gif">

</div>

<script src="script.js"></script>

</body>
</html>
