<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="../../style.css">
</head>

<body id="console">

<?php
  include "../limits.php";
  include "../graphics.php";
  include "../output.php";

  if (isset($_GET["n"]) && isset($_GET["k"]) && isset($_GET["a"]) && isset($_GET["p"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $n = min(intval($_GET["n"]), 20);  // hard-wired maximum combination length
    $fmt = intval($_GET["fmt"]);
    $cmd = "../../code/comb/comb"
                             . " -n" . strval($n)
                             . " -k" . escapeshellarg($_GET["k"])
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of combinations */
                             . " -a" . escapeshellarg($_GET["a"])
                             . " -p" . escapeshellarg($_GET["p"])
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    if ($_GET["p"] == "0") {
      output($result, $fmt, "string_to_bits", 0, "svg_bits", 0, "svg_wheel", "bits_col", $num, $gfx);
    } else {
      output($result, $fmt, "string_to_indicator", $n, "svg_bits", 0, "svg_wheel", "bits_col", $num, $gfx);
    }
  }
?>

</body>
</html>
