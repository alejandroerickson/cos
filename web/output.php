<?php
  // convert string to list of numbers
  function string_to_list($string, $dummy) {
    // is this really a string of numbers separated by whitespace
    if (!preg_match("/^[0-9\s]*$/", $string)) {
      return false;
    }
    // strip leading and trailing whitespace
    $string = preg_replace("/(^\s*|\s*$)/", "", $string);
    if (strlen($string) == 0) {
      return array();
    }
    // split at whitespace
    return array_map('intval', preg_split("/\s+/", $string));
  }

  // convert string to array of bits
  function string_to_bits($string, $dummy) {
    // is this really a bitstring, i.e., a string
    // with only 0s, 1s and whitespace
    if (!preg_match("/^[01\s]*$/", $string)) {
      return false;
    }
    // strip leading and trailing whitespace
    $string = preg_replace("/(^\s*|\s*$)/", "", $string);
    if (strlen($string) == 0) {
      return array();
    }
    // split into individual bit characters
    return array_map('intval', str_split($string));
  }

  // convert string to number partition
  function string_to_partition($string, $dummy) {
    // is this really a number partition, i.e., a string
    // with of numbers separated by |
    if (!preg_match("/^[0-9\s\|]*$/", $string)) {
      return false;
    }
    // split into blocks of sets
    $blocks = preg_split('/\|/', $string);
    $part = array();
    foreach ($blocks as $set) {
      // strip leading and trailing whitespace
      $set = preg_replace("/(^\s*|\s*$)/", "", $set);
      array_push($part, array_map('intval', preg_split("/\s+/", $set)));
    }
    return $part;
  }

  // check if this string represents Lyndon brackets
  // (currently there is no visualization available for them, so
  // we do not really read the object in case of success)
  function string_to_brackets($string, $dummy) {
    if (!preg_match("/^[0-9\s,\[\]]*$/", $string)) {
      return false;
    }
    return true;
  }

  // convert string to Dyck path
  function string_to_dyck($string, $dummy) {
    // is this really a Dyck path, i.e., a string
    // with the number of flaws, then the | separator, then a bitstring
    if (!preg_match("/^[0-9\s]*\|[01\s]*$/", $string)) {
      return false;
    }
    // split into two blocks
    $blocks = preg_split('/\|/', $string);
    return string_to_bits($blocks[1], $dummy);
  }

  // convert string describing a set to array of bits (indicator array)
  function string_to_indicator($string, $n) {
    $set = string_to_list($string, 0);
    if ($set == false) {
      return false;
    }
    return set_to_indicator($set, $n, 1);
  }

  // convert subset representation to indicator array representation
  function set_to_indicator($set, $n, $rebase) {
    $bits = array_fill(0, $n, 0);
    for ($i = 0; $i < sizeof($set); $i++) {
      $bits[$set[$i] - $rebase] = 1;
    }
    return $bits;
  }

  // convert string to array of bits representation of numerical semigroup
  function string_to_sgroup($string, $genus) {
    $set = string_to_list($string, 0);
    if ($set == false) {
      return false;
    }
    // fill set in the end
    $min = $set[sizeof($set) - 1] + 1;
    $max = 2*$genus + 2;
    for ($i = $min; $i <= $max; $i++) {
      array_push($set, $i);
    }
    return set_to_indicator($set, $max+1, 0);
  }

  function parse_objects($result, $fconv, $par) {
    $objects = array();
    $lines = array_fill(0, sizeof($result), 0);
    for ($i = 0; $i < sizeof($result); $i++) {
      $line = $result[$i];
      $obj = $fconv($line, $par);
      if ($obj == false) {
        continue;  // not a valid output object, skip it
      }
      array_push($objects, $obj);
      $lines[$i] = 1;
    }
    return array($objects, $lines);
  }

  function output($result, $fmt, $fconv, $par1, $fsvg_obj, $par2, $fsvg_all, $par3, $num, $gfx) {
    if ($fmt == 0) {  // graphical output
      $p = parse_objects($result, $fconv, $par1);
      $objects = $p[0];
      $lines = $p[1];
      $dtxt = $gfx ? "none" : "";
      $dgfx = $gfx ? "" : "none";
      $dnum = $num ? "" : "none";
      $count = 0;
      for ($i = 0; $i < sizeof($lines); $i++) {
        if ($lines[$i] == 0) {
          echo $result[$i] . "<br>\n";
        } else {
          $count++;
          // output number of object
          echo "<span class='num' style='display:" . $dnum . "'>" . strval($count) . ":&nbsp;</span>";
          // print textual and graphical output
          if ($fsvg_obj !== 0) {
            echo "<span class='txt' style='display:" . $dtxt . "'>" . $result[$i] . "</span>";
            echo "<span class='gfx' style='display:" . $dgfx . "'>";
            echo $fsvg_obj($objects[$i], $par2);
            echo "</span><br>\n";
          } else {
            echo $result[$i] . "<br>";
          }
        }
      }
      if ($count > 0) {
        echo "total = " . strval($count) . "<br>";
      }
      if ($fsvg_all !== 0) {
        echo "<span class='gfx' style='display:" . $dgfx . "'>";
        $fsvg_all($objects, $par3);
        echo "</span><span class='txt'>&nbsp;</span><br>\n";
      }
    } else if ($fmt == 1) {  // text-only output
      foreach ($result as $line) {
        echo $line . "<br>\n";
      }
    } else if ($fmt == 2) {  // file download
      exec("zip file.zip file.txt");
      header("Content-disposition: attachment; filename=file.zip");
      header("Content-type: application/zip");
      readfile("file.zip");
    }
  }
?>
