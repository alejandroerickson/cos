<h1>Binary words with given trace and subtrace</h1>

Here we consider the number $S(n;t,s)$ of length $n$ words $a_1 a_2\ldots a_n$ over the alphabet $\{0,1,\ldots,k-1\}$ with $k=2$ that have trace $t$ and subtrace $s$.
The <span class="def">trace</span> of a $k$-ary word is the sum of its digits mod $k$, i.e., $t=a_1+a_2+\cdots+a_n \pmod{k}$.
The <span class="def">subtrace</span> is the sum of the products of all $n(n-1)/2$ pairs of digits taken mod $k$, i.e., $s=\sum_{1\leq i\lt j\leq n} a_i a_j$.

<p>
<table class="data"><tr>
<td>
<td align="center" colspan="4">(trace,subtrace)</td>
<tr>  
<td align="center">$n$
<td class="special" align="center">(0,0)
<td class="special" align="center">(0,1)
<td class="special" align="center">(1,0)
<td class="special" align="center">(1,1)
<tr><th align="center" class="special">1</th>
<td align="right">1<td align="right">0<td align="right">1
<td align="right">0
<tr><th align="center" class="special">2</th>
<td align="right">1<td align="right">1<td align="right">2
<td align="right">0
<tr><th align="center" class="special">3</th>
<td align="right">1<td align="right">3<td align="right">3
<td align="right">1
<tr><th align="center" class="special">4</th>
<td align="right">2<td align="right">6<td align="right">4
<td align="right">4
<tr><th align="center" class="special">5</th>
<td align="right">6<td align="right">10<td align="right">6
<td align="right">10
<tr><th align="center" class="special">6</th>
<td align="right">16<td align="right">16<td align="right">12
<td align="right">20
<tr><th align="center" class="special">7</th>
<td align="right">36<td align="right">28<td align="right">28
<td align="right">36
<tr><th align="center" class="special">8</th>
<td align="right">72<td align="right">56<td align="right">64
<td align="right">64
<tr><th align="center" class="special">9</th>
<td align="right">136<td align="right">120<td align="right">136
<td align="right">120
<tr><th align="center" class="special">10</th>
<td align="right">256<td align="right">256<td align="right">272
<td align="right">240
<tr><th align="center" class="special">11</th>
<td align="right">496<td align="right">528<td align="right">528
<td align="right">496
<tr><th align="center" class="special">12</th>
<td align="right">992<td align="right">1056<td align="right">1024
<td align="right">1024
<tr><th align="center" class="special">13</th>
<td align="right">2016<td align="right">2080<td align="right">2016
<td align="right">2080
<tr><th align="center" class="special">14</th>
<td align="right">4096<td align="right">4096<td align="right">4032
<td align="right">4160
<tr><th align="center" class="special">15</th>
<td align="right">8256<td align="right">8128<td align="right">8128
<td align="right">8256
<tr><th align="center" class="special">16</th>
<td align="right">16512<td align="right">16256<td align="right">16384
<td align="right">16384
<tr><th align="center" class="special">17</th>
<td align="right">32896<td align="right">32640<td align="right">32896
<td align="right">32640
<tr><th align="center" class="special">18</th>
<td align="right">65536<td align="right">65536<td align="right">65792
<td align="right">65280
<tr><th align="center" class="special">19</th>
<td align="right">130816<td align="right">131328<td align="right">131328
<td align="right">130816
<tr><th align="center" class="special">20</th>
<td align="right">261632<td align="right">262656<td align="right">262144
<td align="right">262144
</table>

<h2>Examples</h2> 

The two binary strings of trace 0, subtrace 0 and length 4 are $\{0000, 1111\}$.  
The two binary strings of trace 1, subtrace 0 and length 2 are $\{10, 01\}$.
The three binary strings of trace 0, subtrace 1 and length 3 are $\{ 011, 101, 110\}$.
The four binary strings of trace 1, subtrace 1 and length 4 are $\{0111, 1011, 1101, 1110\}$.

<h2>Enumeration (OEIS)</h2>

<ul>
<li>
The number $S(n;t,s)$ can be computed from the following recurrence relation
\begin{align}
S(n;t,s) &= S(n-1;t,s) + S(n-1;t-1,s-(t-1)) \\
         &= S(n-1;t,s) + S(n-1;t+1,s+t+1)).
\end{align}

<li>
Column (0,0) is <a href="http://oeis.org/A038503">OEIS A038503</a>.
The value is $\binom{n}{0}+\binom{n}{4}+\binom{n}{8}+\cdots$ which has the closed form expression $(2^n+(1+i)^n+(1-i)^n)/4$, where $i=\sqrt{-1}$.
The $i$ can be eliminated to get $(2^n+2 \sqrt{2}^n \cos(\pi n/4)/4$.
The sequence has the rational generating function $(1-x)^3 /((1-x)^4 -x^4)$.

<li>
Column (0,1) is <a href="http://oeis.org/A038505">OEIS A038505</a>.
The value is $\binom{n}{2}+\binom{n}{6}+\binom{n}{10}+\cdots$ which has the closed form expression $(2^n-(1+i)^n -(1-i)^n)/4$, where $i=\sqrt{-1}$.
The $i$ can be eliminated to get $(2^n-2 \sqrt{2}^n \cos(\pi n/4)/4$.
The sequence has the rational generating function $x^2 (1-x)/((1-x)^4 -x^4)$.

<li>
Column (1,0) is <a href="http://oeis.org/A038504">OEIS A038504</a>.
The value is $\binom{n}{1}+\binom{n}{5}+\binom{n}{9}+\cdots$ which has the closed form expression $(2^n-i(1+i)^n+i(1-i)^n)/4$, wgere $i=\sqrt{-1}$.
The $i$ can be eliminated to get $(2^n+2 \sqrt{2}^n \sin(\pi n/4)/4$.
The sequence has the rational generating function $x(1-x)^2 /((1-x)^4 -x^4)$.

<li>
Column (1,1) is <a href="http://oeis.org/A00749">OEIS A00749</a>.
The value is $\binom{n}{3}+\binom{n}{7}+\binom{n}{11}+\cdots$ which has the closed form expression $(2^n+i(1+i)^n-i(1-i)^n)/4$, where $i=\sqrt{-1}$.
The $i$ can be eliminated to get $(2^n-2 \sqrt{2}^n \sin(\pi n/4)/4$.
The sequence has the rational generating function $x^3 /((1-x)^4 -x^4)$.
</ul>