<?php
  function perm_col($perm, $i) {
    // 20 standard colors
    $pal = array("red", "orange", "gold", "yellow", "greenyellow",
      "springgreen", "limegreen", "forestgreen", "olive", "darkturquoise",
      "aquamarine", "skyblue", "dodgerblue", "royalblue", "darkblue",
      "violet", "mediumpurple", "blueviolet", "purple", "sienna");
    if (($i < 0) || ($i > sizeof($perm)) || ($perm[$i] == 0)) {
      return "dimgray";  // special color for out-of-bounds requests
    }
    $m = max($perm);
    $col = 0;
    if ($m <= 7) {
      $col = 3*($perm[$i] - 1);
    } else if ($m <= 10) {
      $col = 2*($perm[$i] - 1);
    } else {
      $col = ($perm[$i] - 1) % sizeof($pal);
    }
    return $pal[$col];
  }

  // create svg of given permutation
  function svg_perm($perm, $dummy) {
    // each entry of the permutation is drawn as a box with these dimensions
    $box = 12;
    $sw = 1;  // stroke width
    $colbox = "white";

    $n = sizeof($perm);
    echo "<svg width='" . strval($box*$n + $sw) . "' height='" . strval($box + $sw)
       . "' viewbox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval($box*$n + $sw) . " " . strval($box + $sw). "'>";
    echo "<style>.r{stroke:" . $colbox . ";stroke-width:" . strval($sw) . "}</style>";
    for ($i = 0; $i < $n; $i++) {
      echo "<rect class='r' x='" . strval($box*$i) . "' y='0' width='" . strval($box) . "' height='" . strval($box) . "' style='fill:" . perm_col($perm, $i) . "'/>";
    }
    echo "</svg>";
  }

  // print wheel of permutations/bitstrings
  function svg_wheel($objects, $fcol) {
    $strip = 20;  // width of each strip of the wheel
    $ir = 40;  // inner radius of the wheel
    $marks = 5;
    $colbox;
    if ($fcol == "bits_col") {
      $colbox = "lightgray";
    } else {
      $colbox = "white";
    }
    
    $d = sizeof($objects);
    if ($d < 2) {
      return;
    }

    $n = sizeof($objects[0]);
    $or = $ir + $n * $strip;  // outer radius of the wheel
    $box = 2*($or+$marks);  // bounding box

    echo "<svg width='" . strval($box) . "' height='" . strval($box)
       . "' viewbox='" . strval(-0.5*$box) . " " . strval(-0.5*$box) . " " . strval($box) . " " . strval($box). "'>";
    echo "<style>.p{stroke-width:0.6}.m{stroke-width:1;stroke:" . $colbox . "}.c{fill:none;stroke:" . $colbox . "}</style>";
    for ($k = 0; $k < $d; $k++) {
      $x0 = sin(2.0*pi()*$k / $d);
      $y0 = -cos(2.0*pi()*$k / $d);
      $x1 = sin(2.0*pi()*($k+1) / $d);
      $y1 = -cos(2.0*pi()*($k+1) / $d);
      for ($i = 0; $i < $n; $i++) {
        $r0 = $ir + 1.0*($or-$ir)/$n * $i;
        $r1 = $ir + 1.0*($or-$ir)/$n * ($i+1);
        $dr = $r1 - $r0;
        echo "<path class='p' d='m " . strval(round($x0*$r0,2)) . " " . strval(round($y0*$r0,2))
                             . " l " . strval(round($x0*$dr,2)) . " " . strval(round($y0*$dr,2))
                             . " a " . strval(round($r1)) . " " . strval(round($r1)) . " 0 0 1 " . strval(round(($x1-$x0)*$r1,2)) . " " . strval(round(($y1-$y0)*$r1,2))
                             . " l " . strval(round(-$x1*$dr,2)) . " " . strval(round(-$y1*$dr,2))
                             . " a " . strval(round($r0)) . " " . strval(round($r0)) . " 0 0 0 " . strval(round(($x0-$x1)*$r0,2)) . " " . strval(round(($y0-$y1)*$r0,2))
                             . " z' style='stroke:" . $fcol($objects[$k], $i) . ";fill:" . $fcol($objects[$k], $i) . "'/>";
      }
    }
    $s = round($n / ($or - $ir) * (min(max(round(2.0 * $d / pi()), $ir), $or) - $ir));
    for ($i = $s; $i <= $n; $i++) {
      $r = $ir + 1.0*($or-$ir)/$n  * $i;
      echo "<circle class='c' cx='0' cy='0' r='" . strval($r) . "'/>";
    }
    for ($k = 0; $k < $d; $k++) {
      $x0 = sin(2.0*pi()*$k / $d);
      $y0 = -cos(2.0*pi()*$k / $d);
      echo "<path class='m' d='m " . strval(round($x0*($ir + $s*$strip),2)) . " " . strval(round($y0*($ir + $s*$strip),2))
                           . " l " . strval(round($x0*($marks + ($n-$s)*$strip),2)) . " " . strval(round($y0*($marks + ($n-$s)*$strip),2)) . "'/>";
    }

    echo "</svg>";
  }

  // create svg of given meander
  function svg_meander($perm, $dummy) {
    // each arc of the meander is drawn inside a box of these dimensions
    $boxw = 12;
    $boxh = 24;
    $sw = 1.5;  // stroke width
    $rad = 3;  // radius of little dots at start of line
    $col1 = "yellow";
    $col2 = "lightgray";

    $n = sizeof($perm);
    $pos = array_fill(0, $n + 1, -1);  // inverse permutation
    for ($i = 0; $i < $n; $i++) {
      $pos[$perm[$i]] = $i;
    }

    // extract intervals spanned by upper and lower segments
    $upper_ints = array();
    $lower_ints = array();
    for ($i = 1; $i < $n; $i++) {
      $j = $pos[$i];
      $k = $pos[$i+1];
      $l = min($j, $k);
      $r = max($j, $k);
      $int = array($l, $r);
      if ($i % 2 == 1) {
        array_push($lower_ints, $int);
      } else {
        array_push($upper_ints, $int);
      }
    }
    // compute heights and depths when intervals are stacked
    $heights = interval_heights($upper_ints);
    $depths  = interval_heights($lower_ints);
    $max_height = max($heights);
    $max_depth = max($depths);

    // print svg
    $d = max($sw, 2*$rad);  // boundary
    echo "<svg width='" . strval($boxw*($n-1) + $d) . "' height='" . strval($boxh + $sw)
       . "' viewbox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval($boxw*($n-1) + $sw) . " " . strval($boxh + $sw). "'>";
    echo "<style>.l{stroke:" . $col2 . ";stroke-width:" . strval($sw) . "}"
         . ".c{stroke-width:0;fill:" . $col1 . "}"
         . ".p{fill:none;stroke:" . $col1 . ";stroke-width:" . strval($sw) . "}</style>";
    echo "<line class='l' x1='0' y1='" . strval(0.5*$boxh) . "' x2='" . strval($boxw*($n-1)) . "' y2='" . strval(0.5*$boxh) . "'/>";
    echo "<circle class='c' cx='" . strval($boxw*$pos[1]) . "' cy='"  . strval(0.5*$boxh) . "' r='" . strval($rad) . "'/>";
    for ($i = 1; $i < $n; $i++) {
      $j = $pos[$i];
      $k = $pos[$i+1];
      $left = min($j, $k);
      $right = max($j, $k);

      if ($i % 2 == 1) {
        // lower segment
        if ($max_depth == 1) {
          $y = $boxh;
        } else {
          $y = (0.25 * $boxh / ($max_depth - 1)) * ($depths[($i - 1)/2] - 1) + 0.75 * $boxh;
        }
      } else {
        // upper segment
        if ($max_height == 1) {
          $y = 0;
        } else {
          $y = -(0.25 * $boxh / ($max_height - 1)) * ($heights[($i - 2)/2] - 1) + 0.25 * $boxh;
        }
      }
      echo "<polyline class='p' points='" . strval($boxw*$j) . "," . strval(0.5*$boxh) . " "
                                . strval($boxw*$j) . "," . strval($y) . " "
                                . strval($boxw*$k) . "," . strval($y) . " "
                                . strval($boxw*$k) . "," . strval(0.5*$boxh) . "'/>";
    }
    echo "</svg>";
  }

  // for a given set of intervals of non-negative integers, compute
  // the heights of each interval when they are stacked (needed for meanders)
  function interval_heights($ints) {
    $n = sizeof($ints);  // number of intervals
    $max = 0;  // the maximum integer
    // record length of each interval
    $len = array();
    for ($i = 0; $i < $n; $i++) {
      $l = $ints[$i][0];
      $r = $ints[$i][1];
      $max = max($max, $r);
      $len[$i] = $r - $l;
    }
    // sort them in increasing length
    asort($len);
    // keep track of the height of the stacks of intervals
    $stack = array_fill(0, $max+1, 0);
    $heights = array_fill(0, $n, 0);
    foreach ($len as $i => $d) {  // go through intervals by increasing length
      $l = $ints[$i][0];
      $r = $ints[$i][1];
      $h = max(array_slice($stack, $l, $d+1)) + 1;
      $heights[$i] = $h;
      for ($x = $l; $x <= $r; $x++) {
        $stack[$x] = $h;
      }
    }
    return $heights;
  }

  function bits_col($bits, $i) {
    $col0 = "white";
    $col1 = "dimgray";
    $bit = $bits[$i];
    if ($bit == 0) {
      return $col0;
    } else {
      return $col1;
    }
  }

  // create svg of given bitstring
  function svg_bits($bits, $dummy) {
    // each bit is drawn as a box with these dimensions
    $box = 12;
    $sw = 1;  // stroke width
    $colbox = "lightgray";

    $n = sizeof($bits);
    echo "<svg width='" . strval($box*$n + $sw) . "' height='" . strval($box + $sw)
       . "' viewbox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval($box*$n + $sw) . " " . strval($box + $sw). "'>";
    echo "<style>.r{stroke:" . $colbox . ";stroke-width:" . strval($sw) . "}</style>";
    for ($i = 0; $i < $n; $i++) {
      echo "<rect class='r' x='" . strval($box*$i) . "' y='0' width='" . strval($box) . "' height='" . strval($box) . "' style='fill:" . bits_col($bits, $i) . "'/>";
    }
    echo "</svg>";
  }

  function sgroup_col($sgroup, $i) {
    $colnongap = "red";
    $colgap = "dimgray";
    $x = $sgroup[$i];
    if ($x == 1) {
      return $colnongap;
    } else {
      return $colgap;
    }
  }

  // create svg of given gap array of numerical semigroup
  function svg_sgroup($sgroup, $dummy) {
    // each element/gap is drawn as a box with these dimensions
    $box = 12;
    $sw = 1;  // stroke width
    $colbox = "white";

    $n = sizeof($sgroup);
    echo "<svg width='" . strval($box*$n + $sw) . "' height='" . strval($box + $sw)
       . "' viewbox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval($box*$n + $sw) . " " . strval($box + $sw). "'>";
    echo "<style>.r{stroke:" . $colbox . ";stroke-width:" . strval($sw) . "}</style>";
    for ($i = 0; $i < $n; $i++) {
      echo "<rect class='r' x='" . strval($box*$i) . "' y='0' width='" . strval($box) . "' height='" . strval($box) . "' style='fill:" . sgroup_col($sgroup, $i) . "'/>";
    }
    echo "</svg>";
  }

  // create svg of given bitstring as a Dyck path
  function svg_dyck($bits, $dummy) {
    // each bit is drawn as a box with these dimensions
    $box = 12;
    $sw1 = 1.5;  // stroke width
    $sw3 = 2.5;  // stroke width
    $rad = 2.5;
    $col1 = "yellow";
    $col2 = "lightgray";
    $col3 = "red";

    $n = sizeof($bits);
    // compute maximum height and depth of Dyck path
    $max_height = 0;
    $max_depth = 0;
    $h = 0;
    for ($i = 0; $i < $n; $i++) {
      $h += 2*$bits[$i] - 1;
      $max_height = max($max_height, $h);
      $max_depth = min($max_depth, $h);
    }
    $total_height = $max_height - $max_depth;

    $d = max($sw1, 2*$rad);  // boundary
    echo "<svg width='" . strval($box*$n + $d) . "' height='" . strval($box*$total_height + $d)
       . "' viewbox='" . strval(-0.5*$d) . " " . strval(-0.5*$d) . " " . strval($box*$n + $d) . " " . strval($box*$total_height + $d). "'>";
    echo "<style>.l{stroke:" . $col1 . ";stroke-width:" . strval($sw1) . "}.c{stroke-width:0;fill:" . $col1 . "}.f{stroke:" . $col3 . ";stroke-width:" . strval($sw3) . "}</style>";

    $y = $box * $max_height;
    echo "<line x1='0' y1='" . strval($y) . "' x2='" . strval($box*$n) . "' y2='" . strval($y)
       . "' style='stroke:" . $col2 . ";stroke-width:" . strval($sw1) . "'/>";
    $h = 0;
    for ($i = 0; $i < $n; $i++) {
      $d = 2*$bits[$i] - 1;
      $y1 = $box * ($max_height - $h);
      $y2 = $y1 - $box * $d;
      echo "<line class='" . (($h <= 0) && ($bits[$i] == 0) ? "f" : "l") . "' x1='" . strval($box*$i)     . "' y1='" . strval($y1) . "' x2='" . strval($box*($i+1)) . "' y2='" . strval($y2) . "'/>";
      echo "<circle class='c' cx='" . strval($box*$i) . "' cy='"  . strval($y1) . "' r='" . strval($rad) . "'/>";
      if ($i == $n-1) {
        echo "<circle class='c' cx='" . strval($box*($i+1)) . "' cy='"  . strval($y2) . "' r='" . strval($rad) . "'/>";
      }
      $h += $d;
    }
   echo "</svg>";
  }

  // create svg of given tree in level representation
  function svg_tree($tree, $rooted) {
    // each edge of the tree is drawn inside a box of these dimensions
    $boxw = 14;
    $edgeh = 14;
    $sw = 1.5;  // stroke width
    $rad = 2.5;  // radius of nodes
    $col1 = "yellow";
    $col2 = "red";

    $n = sizeof($tree);  // number of nodes
    // count level sizes
    $sizes = array_fill(0, $n, 0);
    for ($i = 0; $i < $n; $i++) {
      $sizes[$tree[$i]]++;
    }

    $boxh = $edgeh * (max($sizes) - 1);

    $d = max($sw, 2*$rad);  // boundary
    echo "<svg width='" . strval($boxw*($n-1) + $d) . "' height='" . strval($boxh + $d)
       . "' viewbox='" . strval(-0.5*$d) . " " . strval(-0.5*$d) . " " . strval($boxw*($n-1) + $d) . " " . strval($boxh + $d). "'>";
    echo "<style>.l{stroke:" . $col1 . ";stroke-width:" . strval($sw) . "}.c{stroke-width:0;fill:" . $col1 . "}</style>";
    $y0 = 0.5*$boxh;

    $levels = array_fill(0, $n, 0);  // count number of nodes on each level
    $ycoord = array_fill(0, $n, 0);  // remember y-coordinate of last node embedded on each level
    $ycoord[0] = $y0;
    for ($i = 1; $i < $n; $i++) {
      $d = $tree[$i];  // depth of node
      $dp = $d - 1;    // depth of parent
      $j  = $levels[$d];   // level index
      $jp = $levels[$dp];  // level index of parent
      if ($sizes[$d] == 1) {  // level contains only a single node
        $y = $y0;
      } else {
        $y  = -1.0*$boxh / ($sizes[$d]  - 1) * $j  + $boxh;
      }
      $yp = $ycoord[$dp];
      // draw edge from node to its parent
      echo "<line class='l' x1='" . strval($boxw*$d)  . "' y1='" . strval($y) . "' x2='" . strval($boxw*$dp) . "' y2='" . strval($yp) . "'/>";
      // draw node itself
      echo "<circle class='c' cx='" . strval($boxw*$d) . "' cy='"  . strval($y) . "' r='" . strval($rad) . "'/>";
      $levels[$d]++;
      $ycoord[$d] = $y;
    }
    // draw root node last
    echo "<circle class='c' cx='0' cy='"  . strval($y0) . "' r='" . strval($rad) . "' style='fill:" . ($rooted ? $col2 : $col1) . "'/>";

    echo "</svg>";
  }

  // create svg with Ferrers diagram of given integer partition
  function svg_ferrers($part, $dummy) {
    // each entry of the permutation is drawn as a box with these dimensions
    $box = 12;
    $sw = 1;  // stroke width
    $col1 = "yellow";
    $col2 = "gray";

    $m = max($part);  // maximum part
    $p = sizeof($part);  // number of parts
    echo "<svg width='" . strval($box*$m + $sw) . "' height='" . strval($box*$p + $sw)
        . "' viewbox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval($box*$m + $sw) . " " . strval($box*$p + $sw). "'>";
    echo "<style>.r{stroke:" . $col2 . ";fill:" . $col1 . ";stroke-width:" . strval($sw) . "}</style>";
    for ($i = 0; $i < $p; $i++) {
      $x = $part[$i];
      for ($j = 0; $j < $x; $j++) {
        echo "<rect class='r' x='" . strval($box*$j) . "' y='". strval($box*$i) . "' width='" . strval($box) . "' height='" . strval($box) . "'/>";
      }
    }
    echo "</svg>";
  }
?>
