<?php
  function limit($fmt) {
    $limit = 0;
    if ($fmt == 0) {  // graphical output
      $limit = 500;
    } else if ($fmt == 1) {  // text-only output
      $limit = 10000;
    } else if ($fmt == 2) {  // file download
      $limit = 100000;
    }
    return $limit;
  }

  function limit_dyck($fmt) {
    $limit = 0;
    if ($fmt == 0) {  // graphical output
      $limit = 6;
    } else if ($fmt == 1) {  // text-only output
      $limit = 8;
    } else if ($fmt == 2) {  // file download
      $limit = 10;
    }
    return $limit;
  }

  function limit_bruijn($k) {
    $n = 1;
    if ($k == 4) {
      $n = 7;
    } else if ($k == 3) {
      $n = 9;
    } else if ($k == 2) {
      $n = 15;
    }
    return $n;
  }
?>
