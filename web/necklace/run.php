<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="../../style.css">
</head>

<body id="console">

<?php
  include "../limits.php";
  include "../graphics.php";
  include "../output.php";

  if (isset($_GET["a"]) && isset($_GET["n"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $a = intval($_GET["a"]);
    $fmt = intval($_GET["fmt"]);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);

    // set up algorithm parameters
    $cmd = "../../code/necklace/necklace "
           . " " . $a
           . " " . min(intval($_GET["n"]), 20)  /* hard-wired maximum string length */
           . " " . intval($_GET["k"])
           . " " . strval(limit($fmt));  /* hard-wired maximum number of objects */

    if ($a == 2 || $a == 22) {
      $cmd = $cmd . " " . intval($_GET["d"]);
    } else if ($a == 3 || $a == 6 || $a == 9 || $a == 23 || $a == 26) {
      $a = string_to_list($_GET["c"], 0);
      if ($a != false) {
      	$cmd = $cmd . " " . implode(" ", $a);
      }
    } else if ($a == 4 || $a == 24) {
      $a = string_to_list($_GET["f"], 0);
      if ($a == false) {
        echo "invalid forbidden substring<br>";
      } else {
        $cmd = $cmd . " "  . strval(sizeof($a)) . " " . implode(" ", $a);
      }
    }

    // redirect stderr to stdout
    $cmd = $cmd . " 2>&1";
    // redirect to output file
    if ($fmt == 2) {
      $cmd = $cmd . " > file.txt";
    }

    exec($cmd, $result);
    if ($a == 11) {  // Lyndon brackets are special, we have no visualization currently
      output($result, $fmt, "string_to_brackets", 0, 0, 0, 0, 0, $num, $gfx);
    } else {
      output($result, $fmt, "string_to_list", 0, "svg_perm", 0, "svg_wheel", "perm_col", $num, $gfx);
    }
  }
?>

</body>
</html>
