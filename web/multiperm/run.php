<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="../../style.css">
</head>

<body id="console">

<?php
  include "../limits.php";
  include "../graphics.php";
  include "../output.php";
  
  if (isset($_GET["f"]) && isset($_GET["a"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    // parse the frequency sequence string and trim it so that the sum of values is at most 20 (hard-wired maximum)
    $fstring = preg_replace("/[^0-9\s]/", "", $_GET["f"]);  // remove everything except digits and whitespace
    $fstring = preg_replace('/\s+/', ' ', trim($fstring));  // remove leading/trailing, condense intermediate whitespace
    $fints = array_map('intval', explode(' ', $fstring));  // convert string to array of integers
    $newfstring = '';
    $sum = 0;
    // rebuild string by taking only the first integers summing to at most 20
    for ($i = 0; $i < count($fints); $i++) {
      $fi = $fints[$i];
      if ($sum + $fi <= 20) {
        $sum += $fi;
        $newfstring = $newfstring . " " . strval($fi);
      }
    }
    $fmt = intval($_GET["fmt"]);
    $cmd = "../../code/multiperm/multiperm"
                             . " -f \"" . $newfstring . "\""  /* the frequency sequence as a string */
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of permutations */
                             . " -a" . escapeshellarg($_GET["a"])
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    output($result, $fmt, "string_to_list", 0, "svg_perm", 0, "svg_wheel", "perm_col", $num, $gfx);
  }
?>

</body>
</html>
