<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="../../style.css">
</head>

<body id="console">

<?php
  include "../limits.php";
  include "../graphics.php";
  include "../output.php";

  if (isset($_GET["n"]) && isset($_GET["a"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $fmt = intval($_GET["fmt"]);
    $cmd = "../../code/part/part"
                             . " -n" . min(intval($_GET["n"]), 15)  /* hard-wired maximum partition length */
                             . (isset($_GET["m"]) ? " -m" . escapeshellarg($_GET["m"]) : "")
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of partitions */
                             . " -a" . escapeshellarg($_GET["a"])
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);

    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    if ($_GET["a"] == "0") {
      output($result, $fmt, "string_to_list", 0, "svg_ferrers", 0, 0, 0, $num, $gfx);
    } else {
      output($result, $fmt, "string_to_partition", 0, 0, 0, 0, 0, $num, $gfx);
    }
  }
?>

</body>
</html>
