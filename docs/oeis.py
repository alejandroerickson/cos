# Script to search OEIS for particular search strings and view all
# hyperlinks contained in lines matching those search strings.
# Results are written to output file out.tsv (tab separated)
import urllib2
import sys
import re
from operator import itemgetter

if (len(sys.argv) < 2):
    print 'Usage: oeis.py [search_string]'
    sys.exit(1)

start = 0
max_start = 0
s = sys.argv[1]
cos_links = []
while (start <= max_start):
  print "**** SCANNING PAGE " + str(start/10) + " ****"
  url = 'http://oeis.org/search?q=' + s + '&start=' + str(start)
  html = urllib2.urlopen(url)
  seq = ''
  sec = ''

  for line in html.read().splitlines():
    # strip whitespace
    line = line.strip()
  
    # search for max page
    m = re.search('<a href="/search\?q=.+>([0-9]+)</a>', line)
    if m:
      max_start = max(max_start, 10*(int(m.group(1)) - 1))
    
    # search for sequence number
    m = re.search('<a href="/(A[0-9]+)">(A[0-9]+)</a>', line)
    if m:
      seq = m.group(2)
    
    # search for subsection heading
    m = re.search('<font size=-2>([A-Z]+)</font>', line)
    if m:
      sec = m.group(1)
  
    # skip matches that are not actual OEIS sequence entries
    if (seq == '') or (sec == ''):
      continue
    if re.search('/search\?q=', line):
      continue
  
    # search for search string
    m = re.search(s, line, re.IGNORECASE)
    if m:  
      # check if line contains a URL
      m = re.search('<a href="(.+)">(.+)</a>', line)
      if m:
        url = m.group(1)
        link = m.group(2)
        cos_links.append([url, link, seq, sec])
   
  start += 10

print "**** WRITING OUTPUT FILE out.tsv ****"
f = open("out.tsv", "w+")
cos_links = sorted(cos_links, key=itemgetter(0))
for [url,link,seq,sec] in cos_links:
  f.write(url + '\t' + link + '\t' + seq + '\t' + sec + '\n')
f.close()