<html>
<?php include "head.html"; ?>

<?php include "web/limits.php"; ?>
<?php $object = $_GET["obj"]; ?>

<body>
<nav id="nav0" class="<?php echo $object; ?>"></nav>

<div id="algo">

<!-- algorithm input -->
<?php include "web/" . $object . "/header.html"; ?>
<div id="input">
<?php include "web/" . $object . "/run.html"; ?>
</div>

<!-- algorithm output -->
<div id="output">
<span class="h2">Output</span>&nbsp;
<span id="options">
<input type="checkbox" id="numbering" onclick="numbering()" checked/> numbering
<input type="checkbox" id="graphics" onclick="graphics()" checked/> graphics
</span><br>
<iframe id="consoleframe" src="<?php echo "web/" . $object . "/run.php"; ?>"></iframe>
</div>

<!-- collapsible subsections -->
<?php
  $sections = ["More information", "Enumeration (OEIS)", "Download source code", "References"];
  $files = ["info.html", "oeis.html", "download.html", "refs.html"];
  for ($i = 0; $i < sizeof($sections); $i++) {
    $file = "web/" . $object . "/" . $files[$i];
    if (file_exists($file)) {
      echo "<h2 class='collapse_heading'>" . $sections[$i] . "</h2>\n";
      echo "<div class='collapse_section'>\n";
      include $file;
      echo "</div>\n\n";
    }
  }
?>

</div>

<script src="script.js"></script>
<script> output_buttons(); </script>

</body>
</html>